<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.0.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="4" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X05">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-6.4262" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
</package>
<package name="1X05/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.35" y1="-1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.985" x2="-5.08" y2="1.27" width="0.762" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="6.985" x2="5.08" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-5.08" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="0" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-6.985" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="8.255" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.461" y1="0.635" x2="-4.699" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="4.699" y1="0.635" x2="5.461" y2="1.143" layer="21"/>
<rectangle x1="-5.461" y1="-2.921" x2="-4.699" y2="-1.905" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
<rectangle x1="4.699" y1="-2.921" x2="5.461" y2="-1.905" layer="21"/>
</package>
<package name="1_05X2MM">
<description>CON-M-1X5-200</description>
<text x="-4.5" y="1.5" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.75" y="-2.75" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5" y1="0.5" x2="-4.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="1" x2="-3.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-3.5" y1="1" x2="-3" y2="0.5" width="0.1524" layer="21"/>
<wire x1="-3" y1="-0.5" x2="-3.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-3.5" y1="-1" x2="-4.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-1" x2="-5" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="-5" y1="0.5" x2="-5" y2="-0.5" width="0.1524" layer="21"/>
<pad name="1" x="-4" y="0" drill="1.016" diameter="1.3" shape="square" rot="R90"/>
<rectangle x1="-4.254" y1="-0.254" x2="-3.746" y2="0.254" layer="51"/>
<wire x1="-3" y1="0.5" x2="-2.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="1" x2="-1.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="1" x2="-1" y2="0.5" width="0.1524" layer="21"/>
<wire x1="-1" y1="-0.5" x2="-1.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-1" x2="-2.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="-1" x2="-3" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="-3" y1="0.5" x2="-3" y2="-0.5" width="0.1524" layer="21"/>
<pad name="3" x="-2" y="0" drill="1.016" diameter="1.3" rot="R90"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<wire x1="-1" y1="0.5" x2="-0.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="1" x2="0.5" y2="1" width="0.1524" layer="21"/>
<wire x1="0.5" y1="1" x2="1" y2="0.5" width="0.1524" layer="21"/>
<wire x1="1" y1="-0.5" x2="0.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="0.5" y1="-1" x2="-0.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="-1" x2="-1" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.1524" layer="21"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.3" rot="R90"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<wire x1="1" y1="0.5" x2="1.5" y2="1" width="0.1524" layer="21"/>
<wire x1="1.5" y1="1" x2="2.5" y2="1" width="0.1524" layer="21"/>
<wire x1="2.5" y1="1" x2="3" y2="0.5" width="0.1524" layer="21"/>
<wire x1="3" y1="-0.5" x2="2.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="2.5" y1="-1" x2="1.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-1" x2="1" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="1" y1="0.5" x2="1" y2="-0.5" width="0.1524" layer="21"/>
<pad name="4" x="2" y="0" drill="1.016" diameter="1.3" rot="R90"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<wire x1="3" y1="0.5" x2="3.5" y2="1" width="0.1524" layer="21"/>
<wire x1="3.5" y1="1" x2="4.5" y2="1" width="0.1524" layer="21"/>
<wire x1="4.5" y1="1" x2="5" y2="0.5" width="0.1524" layer="21"/>
<wire x1="5" y1="0.5" x2="5" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="5" y1="-0.5" x2="4.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-1" x2="3.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="3.5" y1="-1" x2="3" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="3" y1="0.5" x2="3" y2="-0.5" width="0.1524" layer="21"/>
<pad name="5" x="4" y="0" drill="1.016" diameter="1.3" rot="R90"/>
<rectangle x1="3.746" y1="-0.254" x2="4.254" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINHD5">
<wire x1="-6.35" y1="-7.62" x2="1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X5" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X05">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X05/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5X2MM" package="1_05X2MM">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A5L-LOC">
<wire x1="85.09" y1="3.81" x2="85.09" y2="24.13" width="0.1016" layer="94"/>
<wire x1="85.09" y1="24.13" x2="139.065" y2="24.13" width="0.1016" layer="94"/>
<wire x1="139.065" y1="24.13" x2="180.34" y2="24.13" width="0.1016" layer="94"/>
<wire x1="170.18" y1="3.81" x2="170.18" y2="8.89" width="0.1016" layer="94"/>
<wire x1="170.18" y1="8.89" x2="180.34" y2="8.89" width="0.1016" layer="94"/>
<wire x1="170.18" y1="8.89" x2="139.065" y2="8.89" width="0.1016" layer="94"/>
<wire x1="139.065" y1="8.89" x2="139.065" y2="3.81" width="0.1016" layer="94"/>
<wire x1="139.065" y1="8.89" x2="139.065" y2="13.97" width="0.1016" layer="94"/>
<wire x1="139.065" y1="13.97" x2="180.34" y2="13.97" width="0.1016" layer="94"/>
<wire x1="139.065" y1="13.97" x2="139.065" y2="19.05" width="0.1016" layer="94"/>
<wire x1="139.065" y1="19.05" x2="180.34" y2="19.05" width="0.1016" layer="94"/>
<wire x1="139.065" y1="19.05" x2="139.065" y2="24.13" width="0.1016" layer="94"/>
<text x="140.97" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="140.97" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="154.305" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="140.716" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="184.15" y2="133.35" columns="4" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A5L-LOC" prefix="FRAME" uservalue="yes">
<description>A5L LOC</description>
<gates>
<gate name="G$1" symbol="A5L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.4064" drill="0">
<clearance class="0" value="0.4064"/>
</class>
</classes>
<parts>
<part name="TO_3STATE" library="pinhead" deviceset="PINHD-1X5" device=""/>
<part name="IN" library="pinhead" deviceset="PINHD-1X5" device=""/>
<part name="OUT" library="pinhead" deviceset="PINHD-1X5" device=""/>
<part name="FRAME1" library="frames" deviceset="A5L-LOC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="140.969796875" y="20.32005" size="2.54" layer="94" ratio="20">Viktor Bohuněk 4. C</text>
<rectangle x1="100.485225" y1="6.23198125" x2="102.327325" y2="6.3635625" layer="200"/>
<rectangle x1="99.695725" y1="6.3635625" x2="103.116725" y2="6.4951375" layer="200"/>
<rectangle x1="99.300925" y1="6.495140625" x2="103.511525" y2="6.626721875" layer="200"/>
<rectangle x1="98.906225" y1="6.626721875" x2="101.143125" y2="6.758303125" layer="200"/>
<rectangle x1="101.669425" y1="6.626721875" x2="103.906225" y2="6.758303125" layer="200"/>
<rectangle x1="98.511525" y1="6.758303125" x2="100.090425" y2="6.889878125" layer="200"/>
<rectangle x1="102.722025" y1="6.758303125" x2="104.300925" y2="6.889878125" layer="200"/>
<rectangle x1="98.248325" y1="6.88988125" x2="99.432525" y2="7.02145" layer="200"/>
<rectangle x1="103.379925" y1="6.88988125" x2="104.564125" y2="7.02145" layer="200"/>
<rectangle x1="97.985225" y1="7.021453125" x2="99.037825" y2="7.153028125" layer="200"/>
<rectangle x1="103.774625" y1="7.021453125" x2="104.827325" y2="7.153028125" layer="200"/>
<rectangle x1="97.722025" y1="7.15303125" x2="98.774625" y2="7.2846125" layer="200"/>
<rectangle x1="101.143125" y1="7.15303125" x2="101.669425" y2="7.2846125" layer="200"/>
<rectangle x1="104.037825" y1="7.15303125" x2="105.090425" y2="7.2846125" layer="200"/>
<rectangle x1="97.458825" y1="7.2846125" x2="98.379925" y2="7.4161875" layer="200"/>
<rectangle x1="100.879925" y1="7.2846125" x2="101.932525" y2="7.4161875" layer="200"/>
<rectangle x1="104.432525" y1="7.2846125" x2="105.353625" y2="7.4161875" layer="200"/>
<rectangle x1="97.327325" y1="7.416190625" x2="98.116725" y2="7.547771875" layer="200"/>
<rectangle x1="100.879925" y1="7.416190625" x2="101.274625" y2="7.547771875" layer="200"/>
<rectangle x1="101.537825" y1="7.416190625" x2="101.932525" y2="7.547771875" layer="200"/>
<rectangle x1="102.985225" y1="7.416190625" x2="103.379925" y2="7.547771875" layer="200"/>
<rectangle x1="104.695725" y1="7.416190625" x2="105.485225" y2="7.547771875" layer="200"/>
<rectangle x1="97.064125" y1="7.547771875" x2="97.985225" y2="7.679353125" layer="200"/>
<rectangle x1="99.037825" y1="7.547771875" x2="99.958825" y2="7.679353125" layer="200"/>
<rectangle x1="100.879925" y1="7.547771875" x2="101.143125" y2="7.679353125" layer="200"/>
<rectangle x1="101.669425" y1="7.547771875" x2="102.064125" y2="7.679353125" layer="200"/>
<rectangle x1="102.985225" y1="7.547771875" x2="103.774625" y2="7.679353125" layer="200"/>
<rectangle x1="104.827325" y1="7.547771875" x2="105.748325" y2="7.679353125" layer="200"/>
<rectangle x1="96.932525" y1="7.679353125" x2="97.722025" y2="7.810928125" layer="200"/>
<rectangle x1="98.774625" y1="7.679353125" x2="99.958825" y2="7.810928125" layer="200"/>
<rectangle x1="100.879925" y1="7.679353125" x2="101.143125" y2="7.810928125" layer="200"/>
<rectangle x1="101.669425" y1="7.679353125" x2="102.064125" y2="7.810928125" layer="200"/>
<rectangle x1="102.853625" y1="7.679353125" x2="104.037825" y2="7.810928125" layer="200"/>
<rectangle x1="105.090425" y1="7.679353125" x2="105.879925" y2="7.810928125" layer="200"/>
<rectangle x1="96.669425" y1="7.81093125" x2="97.458825" y2="7.9425125" layer="200"/>
<rectangle x1="98.774625" y1="7.81093125" x2="99.169425" y2="7.9425125" layer="200"/>
<rectangle x1="99.695725" y1="7.81093125" x2="100.090425" y2="7.9425125" layer="200"/>
<rectangle x1="100.879925" y1="7.81093125" x2="101.143125" y2="7.9425125" layer="200"/>
<rectangle x1="101.669425" y1="7.81093125" x2="102.064125" y2="7.9425125" layer="200"/>
<rectangle x1="102.853625" y1="7.81093125" x2="103.248325" y2="7.9425125" layer="200"/>
<rectangle x1="103.643125" y1="7.81093125" x2="104.037825" y2="7.9425125" layer="200"/>
<rectangle x1="105.353625" y1="7.81093125" x2="106.011525" y2="7.9425125" layer="200"/>
<rectangle x1="96.537825" y1="7.9425125" x2="97.327325" y2="8.0740875" layer="200"/>
<rectangle x1="98.774625" y1="7.9425125" x2="99.169425" y2="8.0740875" layer="200"/>
<rectangle x1="99.827325" y1="7.9425125" x2="100.222025" y2="8.0740875" layer="200"/>
<rectangle x1="100.879925" y1="7.9425125" x2="101.143125" y2="8.0740875" layer="200"/>
<rectangle x1="101.669425" y1="7.9425125" x2="102.064125" y2="8.0740875" layer="200"/>
<rectangle x1="102.853625" y1="7.9425125" x2="103.116725" y2="8.0740875" layer="200"/>
<rectangle x1="103.774625" y1="7.9425125" x2="104.037825" y2="8.0740875" layer="200"/>
<rectangle x1="105.485225" y1="7.9425125" x2="106.274625" y2="8.0740875" layer="200"/>
<rectangle x1="96.406225" y1="8.074090625" x2="97.195725" y2="8.205659375" layer="200"/>
<rectangle x1="97.853625" y1="8.074090625" x2="97.985225" y2="8.205659375" layer="200"/>
<rectangle x1="98.906225" y1="8.074090625" x2="99.169425" y2="8.205659375" layer="200"/>
<rectangle x1="99.827325" y1="8.074090625" x2="100.222025" y2="8.205659375" layer="200"/>
<rectangle x1="100.879925" y1="8.074090625" x2="101.143125" y2="8.205659375" layer="200"/>
<rectangle x1="101.669425" y1="8.074090625" x2="102.064125" y2="8.205659375" layer="200"/>
<rectangle x1="102.722025" y1="8.074090625" x2="103.248325" y2="8.205659375" layer="200"/>
<rectangle x1="103.774625" y1="8.074090625" x2="104.037825" y2="8.205659375" layer="200"/>
<rectangle x1="105.616725" y1="8.074090625" x2="106.406225" y2="8.205659375" layer="200"/>
<rectangle x1="96.274625" y1="8.2056625" x2="96.932525" y2="8.3372375" layer="200"/>
<rectangle x1="97.722025" y1="8.2056625" x2="97.985225" y2="8.3372375" layer="200"/>
<rectangle x1="98.906225" y1="8.2056625" x2="99.300925" y2="8.3372375" layer="200"/>
<rectangle x1="99.958825" y1="8.2056625" x2="100.222025" y2="8.3372375" layer="200"/>
<rectangle x1="100.879925" y1="8.2056625" x2="101.143125" y2="8.3372375" layer="200"/>
<rectangle x1="101.669425" y1="8.2056625" x2="102.064125" y2="8.3372375" layer="200"/>
<rectangle x1="102.722025" y1="8.2056625" x2="104.037825" y2="8.3372375" layer="200"/>
<rectangle x1="105.879925" y1="8.2056625" x2="106.537825" y2="8.3372375" layer="200"/>
<rectangle x1="96.143125" y1="8.337240625" x2="96.800925" y2="8.468821875" layer="200"/>
<rectangle x1="97.722025" y1="8.337240625" x2="97.985225" y2="8.468821875" layer="200"/>
<rectangle x1="98.906225" y1="8.337240625" x2="99.300925" y2="8.468821875" layer="200"/>
<rectangle x1="99.958825" y1="8.337240625" x2="100.222025" y2="8.468821875" layer="200"/>
<rectangle x1="100.879925" y1="8.337240625" x2="101.143125" y2="8.468821875" layer="200"/>
<rectangle x1="101.669425" y1="8.337240625" x2="102.064125" y2="8.468821875" layer="200"/>
<rectangle x1="102.722025" y1="8.337240625" x2="102.985225" y2="8.468821875" layer="200"/>
<rectangle x1="103.248325" y1="8.337240625" x2="103.906225" y2="8.468821875" layer="200"/>
<rectangle x1="104.827325" y1="8.337240625" x2="105.222025" y2="8.468821875" layer="200"/>
<rectangle x1="106.011525" y1="8.337240625" x2="106.669425" y2="8.468821875" layer="200"/>
<rectangle x1="96.011525" y1="8.468821875" x2="96.669425" y2="8.600403125" layer="200"/>
<rectangle x1="97.722025" y1="8.468821875" x2="97.985225" y2="8.600403125" layer="200"/>
<rectangle x1="99.037825" y1="8.468821875" x2="99.300925" y2="8.600403125" layer="200"/>
<rectangle x1="99.827325" y1="8.468821875" x2="100.222025" y2="8.600403125" layer="200"/>
<rectangle x1="100.879925" y1="8.468821875" x2="101.143125" y2="8.600403125" layer="200"/>
<rectangle x1="101.669425" y1="8.468821875" x2="102.064125" y2="8.600403125" layer="200"/>
<rectangle x1="102.590425" y1="8.468821875" x2="102.985225" y2="8.600403125" layer="200"/>
<rectangle x1="103.511525" y1="8.468821875" x2="103.774625" y2="8.600403125" layer="200"/>
<rectangle x1="104.695725" y1="8.468821875" x2="105.090425" y2="8.600403125" layer="200"/>
<rectangle x1="106.143125" y1="8.468821875" x2="106.800925" y2="8.600403125" layer="200"/>
<rectangle x1="95.879925" y1="8.600403125" x2="96.537825" y2="8.731978125" layer="200"/>
<rectangle x1="97.722025" y1="8.600403125" x2="97.985225" y2="8.731978125" layer="200"/>
<rectangle x1="99.037825" y1="8.600403125" x2="99.432525" y2="8.731978125" layer="200"/>
<rectangle x1="99.695725" y1="8.600403125" x2="100.222025" y2="8.731978125" layer="200"/>
<rectangle x1="100.879925" y1="8.600403125" x2="101.143125" y2="8.731978125" layer="200"/>
<rectangle x1="101.800925" y1="8.600403125" x2="101.932525" y2="8.731978125" layer="200"/>
<rectangle x1="102.590425" y1="8.600403125" x2="102.985225" y2="8.731978125" layer="200"/>
<rectangle x1="103.511525" y1="8.600403125" x2="103.774625" y2="8.731978125" layer="200"/>
<rectangle x1="104.695725" y1="8.600403125" x2="105.090425" y2="8.731978125" layer="200"/>
<rectangle x1="106.274625" y1="8.600403125" x2="106.932525" y2="8.731978125" layer="200"/>
<rectangle x1="95.748325" y1="8.73198125" x2="96.406225" y2="8.8635625" layer="200"/>
<rectangle x1="97.064125" y1="8.73198125" x2="97.195725" y2="8.8635625" layer="200"/>
<rectangle x1="97.722025" y1="8.73198125" x2="97.985225" y2="8.8635625" layer="200"/>
<rectangle x1="99.037825" y1="8.73198125" x2="100.090425" y2="8.8635625" layer="200"/>
<rectangle x1="102.590425" y1="8.73198125" x2="103.774625" y2="8.8635625" layer="200"/>
<rectangle x1="104.564125" y1="8.73198125" x2="104.958825" y2="8.8635625" layer="200"/>
<rectangle x1="106.406225" y1="8.73198125" x2="107.064125" y2="8.8635625" layer="200"/>
<rectangle x1="95.616725" y1="8.8635625" x2="96.274625" y2="8.9951375" layer="200"/>
<rectangle x1="96.932525" y1="8.8635625" x2="97.327325" y2="8.9951375" layer="200"/>
<rectangle x1="97.722025" y1="8.8635625" x2="98.511525" y2="8.9951375" layer="200"/>
<rectangle x1="99.169425" y1="8.8635625" x2="99.827325" y2="8.9951375" layer="200"/>
<rectangle x1="102.985225" y1="8.8635625" x2="103.774625" y2="8.9951375" layer="200"/>
<rectangle x1="104.432525" y1="8.8635625" x2="104.827325" y2="8.9951375" layer="200"/>
<rectangle x1="106.537825" y1="8.8635625" x2="107.195725" y2="8.9951375" layer="200"/>
<rectangle x1="95.485225" y1="8.995140625" x2="96.143125" y2="9.126721875" layer="200"/>
<rectangle x1="97.064125" y1="8.995140625" x2="97.458825" y2="9.126721875" layer="200"/>
<rectangle x1="97.722025" y1="8.995140625" x2="98.643125" y2="9.126721875" layer="200"/>
<rectangle x1="99.169425" y1="8.995140625" x2="99.432525" y2="9.126721875" layer="200"/>
<rectangle x1="104.432525" y1="8.995140625" x2="104.827325" y2="9.126721875" layer="200"/>
<rectangle x1="106.669425" y1="8.995140625" x2="107.195725" y2="9.126721875" layer="200"/>
<rectangle x1="95.485225" y1="9.126721875" x2="96.011525" y2="9.258303125" layer="200"/>
<rectangle x1="97.195725" y1="9.126721875" x2="97.985225" y2="9.258303125" layer="200"/>
<rectangle x1="98.248325" y1="9.126721875" x2="98.643125" y2="9.258303125" layer="200"/>
<rectangle x1="100.353625" y1="9.126721875" x2="101.011525" y2="9.258303125" layer="200"/>
<rectangle x1="101.274625" y1="9.126721875" x2="101.537825" y2="9.258303125" layer="200"/>
<rectangle x1="101.800925" y1="9.126721875" x2="102.458825" y2="9.258303125" layer="200"/>
<rectangle x1="104.300925" y1="9.126721875" x2="104.695725" y2="9.258303125" layer="200"/>
<rectangle x1="106.800925" y1="9.126721875" x2="107.327325" y2="9.258303125" layer="200"/>
<rectangle x1="95.353625" y1="9.258303125" x2="95.879925" y2="9.389878125" layer="200"/>
<rectangle x1="97.195725" y1="9.258303125" x2="97.853625" y2="9.389878125" layer="200"/>
<rectangle x1="98.379925" y1="9.258303125" x2="98.643125" y2="9.389878125" layer="200"/>
<rectangle x1="99.827325" y1="9.258303125" x2="100.222025" y2="9.389878125" layer="200"/>
<rectangle x1="102.590425" y1="9.258303125" x2="102.985225" y2="9.389878125" layer="200"/>
<rectangle x1="104.300925" y1="9.258303125" x2="104.564125" y2="9.389878125" layer="200"/>
<rectangle x1="105.616725" y1="9.258303125" x2="106.274625" y2="9.389878125" layer="200"/>
<rectangle x1="106.932525" y1="9.258303125" x2="107.458825" y2="9.389878125" layer="200"/>
<rectangle x1="95.222025" y1="9.38988125" x2="95.748325" y2="9.52145" layer="200"/>
<rectangle x1="97.327325" y1="9.38988125" x2="97.722025" y2="9.52145" layer="200"/>
<rectangle x1="98.248325" y1="9.38988125" x2="98.643125" y2="9.52145" layer="200"/>
<rectangle x1="99.564125" y1="9.38988125" x2="99.827325" y2="9.52145" layer="200"/>
<rectangle x1="102.985225" y1="9.38988125" x2="103.248325" y2="9.52145" layer="200"/>
<rectangle x1="104.169425" y1="9.38988125" x2="104.564125" y2="9.52145" layer="200"/>
<rectangle x1="105.485225" y1="9.38988125" x2="106.406225" y2="9.52145" layer="200"/>
<rectangle x1="107.064125" y1="9.38988125" x2="107.590425" y2="9.52145" layer="200"/>
<rectangle x1="95.222025" y1="9.521453125" x2="95.748325" y2="9.653028125" layer="200"/>
<rectangle x1="96.274625" y1="9.521453125" x2="96.406225" y2="9.653028125" layer="200"/>
<rectangle x1="97.458825" y1="9.521453125" x2="97.853625" y2="9.653028125" layer="200"/>
<rectangle x1="98.116725" y1="9.521453125" x2="98.511525" y2="9.653028125" layer="200"/>
<rectangle x1="99.300925" y1="9.521453125" x2="99.564125" y2="9.653028125" layer="200"/>
<rectangle x1="103.248325" y1="9.521453125" x2="103.511525" y2="9.653028125" layer="200"/>
<rectangle x1="104.300925" y1="9.521453125" x2="104.432525" y2="9.653028125" layer="200"/>
<rectangle x1="105.222025" y1="9.521453125" x2="105.879925" y2="9.653028125" layer="200"/>
<rectangle x1="106.143125" y1="9.521453125" x2="106.537825" y2="9.653028125" layer="200"/>
<rectangle x1="107.064125" y1="9.521453125" x2="107.590425" y2="9.653028125" layer="200"/>
<rectangle x1="95.090425" y1="9.65303125" x2="95.616725" y2="9.7846125" layer="200"/>
<rectangle x1="96.143125" y1="9.65303125" x2="96.537825" y2="9.7846125" layer="200"/>
<rectangle x1="97.590425" y1="9.65303125" x2="98.379925" y2="9.7846125" layer="200"/>
<rectangle x1="99.037825" y1="9.65303125" x2="99.300925" y2="9.7846125" layer="200"/>
<rectangle x1="103.511525" y1="9.65303125" x2="103.774625" y2="9.7846125" layer="200"/>
<rectangle x1="105.222025" y1="9.65303125" x2="105.616725" y2="9.7846125" layer="200"/>
<rectangle x1="106.274625" y1="9.65303125" x2="106.537825" y2="9.7846125" layer="200"/>
<rectangle x1="107.195725" y1="9.65303125" x2="107.722025" y2="9.7846125" layer="200"/>
<rectangle x1="94.958825" y1="9.7846125" x2="95.485225" y2="9.9161875" layer="200"/>
<rectangle x1="96.143125" y1="9.7846125" x2="96.537825" y2="9.9161875" layer="200"/>
<rectangle x1="97.722025" y1="9.7846125" x2="98.116725" y2="9.9161875" layer="200"/>
<rectangle x1="98.774625" y1="9.7846125" x2="99.037825" y2="9.9161875" layer="200"/>
<rectangle x1="103.774625" y1="9.7846125" x2="104.037825" y2="9.9161875" layer="200"/>
<rectangle x1="105.090425" y1="9.7846125" x2="105.485225" y2="9.9161875" layer="200"/>
<rectangle x1="106.274625" y1="9.7846125" x2="106.537825" y2="9.9161875" layer="200"/>
<rectangle x1="107.327325" y1="9.7846125" x2="107.853625" y2="9.9161875" layer="200"/>
<rectangle x1="94.958825" y1="9.916190625" x2="95.485225" y2="10.047771875" layer="200"/>
<rectangle x1="96.274625" y1="9.916190625" x2="96.669425" y2="10.047771875" layer="200"/>
<rectangle x1="97.722025" y1="9.916190625" x2="97.985225" y2="10.047771875" layer="200"/>
<rectangle x1="98.643125" y1="9.916190625" x2="98.774625" y2="10.047771875" layer="200"/>
<rectangle x1="104.037825" y1="9.916190625" x2="104.169425" y2="10.047771875" layer="200"/>
<rectangle x1="105.090425" y1="9.916190625" x2="105.353625" y2="10.047771875" layer="200"/>
<rectangle x1="106.274625" y1="9.916190625" x2="106.537825" y2="10.047771875" layer="200"/>
<rectangle x1="107.327325" y1="9.916190625" x2="107.853625" y2="10.047771875" layer="200"/>
<rectangle x1="94.827325" y1="10.047771875" x2="95.353625" y2="10.179353125" layer="200"/>
<rectangle x1="96.274625" y1="10.047771875" x2="96.800925" y2="10.179353125" layer="200"/>
<rectangle x1="98.379925" y1="10.047771875" x2="98.643125" y2="10.179353125" layer="200"/>
<rectangle x1="104.169425" y1="10.047771875" x2="104.432525" y2="10.179353125" layer="200"/>
<rectangle x1="105.090425" y1="10.047771875" x2="105.353625" y2="10.179353125" layer="200"/>
<rectangle x1="106.274625" y1="10.047771875" x2="106.537825" y2="10.179353125" layer="200"/>
<rectangle x1="107.458825" y1="10.047771875" x2="107.985225" y2="10.179353125" layer="200"/>
<rectangle x1="94.827325" y1="10.179353125" x2="95.222025" y2="10.310928125" layer="200"/>
<rectangle x1="96.274625" y1="10.179353125" x2="96.932525" y2="10.310928125" layer="200"/>
<rectangle x1="98.248325" y1="10.179353125" x2="98.511525" y2="10.310928125" layer="200"/>
<rectangle x1="104.300925" y1="10.179353125" x2="104.564125" y2="10.310928125" layer="200"/>
<rectangle x1="105.090425" y1="10.179353125" x2="105.485225" y2="10.310928125" layer="200"/>
<rectangle x1="106.406225" y1="10.179353125" x2="106.537825" y2="10.310928125" layer="200"/>
<rectangle x1="107.590425" y1="10.179353125" x2="107.985225" y2="10.310928125" layer="200"/>
<rectangle x1="94.695725" y1="10.31093125" x2="95.222025" y2="10.4425125" layer="200"/>
<rectangle x1="96.143125" y1="10.31093125" x2="96.932525" y2="10.4425125" layer="200"/>
<rectangle x1="98.116725" y1="10.31093125" x2="98.379925" y2="10.4425125" layer="200"/>
<rectangle x1="104.432525" y1="10.31093125" x2="104.695725" y2="10.4425125" layer="200"/>
<rectangle x1="105.222025" y1="10.31093125" x2="105.485225" y2="10.4425125" layer="200"/>
<rectangle x1="107.590425" y1="10.31093125" x2="108.116725" y2="10.4425125" layer="200"/>
<rectangle x1="94.695725" y1="10.4425125" x2="95.090425" y2="10.5740875" layer="200"/>
<rectangle x1="96.143125" y1="10.4425125" x2="96.406225" y2="10.5740875" layer="200"/>
<rectangle x1="96.669425" y1="10.4425125" x2="97.064125" y2="10.5740875" layer="200"/>
<rectangle x1="97.985225" y1="10.4425125" x2="98.248325" y2="10.5740875" layer="200"/>
<rectangle x1="104.564125" y1="10.4425125" x2="104.827325" y2="10.5740875" layer="200"/>
<rectangle x1="105.222025" y1="10.4425125" x2="105.879925" y2="10.5740875" layer="200"/>
<rectangle x1="107.722025" y1="10.4425125" x2="108.116725" y2="10.5740875" layer="200"/>
<rectangle x1="94.564125" y1="10.574090625" x2="95.090425" y2="10.705659375" layer="200"/>
<rectangle x1="95.616725" y1="10.574090625" x2="96.406225" y2="10.705659375" layer="200"/>
<rectangle x1="96.800925" y1="10.574090625" x2="97.195725" y2="10.705659375" layer="200"/>
<rectangle x1="97.853625" y1="10.574090625" x2="98.116725" y2="10.705659375" layer="200"/>
<rectangle x1="99.827325" y1="10.574090625" x2="102.985225" y2="10.705659375" layer="200"/>
<rectangle x1="104.695725" y1="10.574090625" x2="104.958825" y2="10.705659375" layer="200"/>
<rectangle x1="105.353625" y1="10.574090625" x2="106.011525" y2="10.705659375" layer="200"/>
<rectangle x1="107.722025" y1="10.574090625" x2="108.248325" y2="10.705659375" layer="200"/>
<rectangle x1="94.564125" y1="10.7056625" x2="94.958825" y2="10.8372375" layer="200"/>
<rectangle x1="95.616725" y1="10.7056625" x2="97.195725" y2="10.8372375" layer="200"/>
<rectangle x1="97.722025" y1="10.7056625" x2="97.985225" y2="10.8372375" layer="200"/>
<rectangle x1="99.695725" y1="10.7056625" x2="103.116725" y2="10.8372375" layer="200"/>
<rectangle x1="104.827325" y1="10.7056625" x2="105.090425" y2="10.8372375" layer="200"/>
<rectangle x1="107.853625" y1="10.7056625" x2="108.248325" y2="10.8372375" layer="200"/>
<rectangle x1="94.432525" y1="10.837240625" x2="94.958825" y2="10.968821875" layer="200"/>
<rectangle x1="96.274625" y1="10.837240625" x2="97.195725" y2="10.968821875" layer="200"/>
<rectangle x1="97.722025" y1="10.837240625" x2="97.853625" y2="10.968821875" layer="200"/>
<rectangle x1="99.695725" y1="10.837240625" x2="103.116725" y2="10.968821875" layer="200"/>
<rectangle x1="104.958825" y1="10.837240625" x2="105.090425" y2="10.968821875" layer="200"/>
<rectangle x1="106.932525" y1="10.837240625" x2="107.327325" y2="10.968821875" layer="200"/>
<rectangle x1="107.853625" y1="10.837240625" x2="108.379925" y2="10.968821875" layer="200"/>
<rectangle x1="94.432525" y1="10.968821875" x2="94.827325" y2="11.100403125" layer="200"/>
<rectangle x1="96.932525" y1="10.968821875" x2="97.064125" y2="11.100403125" layer="200"/>
<rectangle x1="97.590425" y1="10.968821875" x2="97.722025" y2="11.100403125" layer="200"/>
<rectangle x1="99.695725" y1="10.968821875" x2="103.116725" y2="11.100403125" layer="200"/>
<rectangle x1="105.090425" y1="10.968821875" x2="105.222025" y2="11.100403125" layer="200"/>
<rectangle x1="106.537825" y1="10.968821875" x2="107.327325" y2="11.100403125" layer="200"/>
<rectangle x1="107.985225" y1="10.968821875" x2="108.379925" y2="11.100403125" layer="200"/>
<rectangle x1="94.300925" y1="11.100403125" x2="94.827325" y2="11.231978125" layer="200"/>
<rectangle x1="97.458825" y1="11.100403125" x2="97.590425" y2="11.231978125" layer="200"/>
<rectangle x1="99.695725" y1="11.100403125" x2="103.116725" y2="11.231978125" layer="200"/>
<rectangle x1="105.222025" y1="11.100403125" x2="105.353625" y2="11.231978125" layer="200"/>
<rectangle x1="106.274625" y1="11.100403125" x2="107.458825" y2="11.231978125" layer="200"/>
<rectangle x1="107.985225" y1="11.100403125" x2="108.379925" y2="11.231978125" layer="200"/>
<rectangle x1="94.300925" y1="11.23198125" x2="94.827325" y2="11.3635625" layer="200"/>
<rectangle x1="97.458825" y1="11.23198125" x2="97.590425" y2="11.3635625" layer="200"/>
<rectangle x1="99.695725" y1="11.23198125" x2="103.116725" y2="11.3635625" layer="200"/>
<rectangle x1="105.222025" y1="11.23198125" x2="105.353625" y2="11.3635625" layer="200"/>
<rectangle x1="106.011525" y1="11.23198125" x2="106.800925" y2="11.3635625" layer="200"/>
<rectangle x1="107.195725" y1="11.23198125" x2="107.458825" y2="11.3635625" layer="200"/>
<rectangle x1="107.985225" y1="11.23198125" x2="108.511525" y2="11.3635625" layer="200"/>
<rectangle x1="94.300925" y1="11.3635625" x2="94.695725" y2="11.4951375" layer="200"/>
<rectangle x1="97.327325" y1="11.3635625" x2="97.458825" y2="11.4951375" layer="200"/>
<rectangle x1="99.695725" y1="11.3635625" x2="103.116725" y2="11.4951375" layer="200"/>
<rectangle x1="105.353625" y1="11.3635625" x2="105.485225" y2="11.4951375" layer="200"/>
<rectangle x1="105.879925" y1="11.3635625" x2="106.800925" y2="11.4951375" layer="200"/>
<rectangle x1="107.327325" y1="11.3635625" x2="107.590425" y2="11.4951375" layer="200"/>
<rectangle x1="108.116725" y1="11.3635625" x2="108.511525" y2="11.4951375" layer="200"/>
<rectangle x1="94.300925" y1="11.495140625" x2="94.695725" y2="11.626721875" layer="200"/>
<rectangle x1="97.195725" y1="11.495140625" x2="97.458825" y2="11.626721875" layer="200"/>
<rectangle x1="99.695725" y1="11.495140625" x2="103.116725" y2="11.626721875" layer="200"/>
<rectangle x1="105.353625" y1="11.495140625" x2="105.616725" y2="11.626721875" layer="200"/>
<rectangle x1="106.011525" y1="11.495140625" x2="106.274625" y2="11.626721875" layer="200"/>
<rectangle x1="106.669425" y1="11.495140625" x2="106.932525" y2="11.626721875" layer="200"/>
<rectangle x1="107.327325" y1="11.495140625" x2="107.590425" y2="11.626721875" layer="200"/>
<rectangle x1="108.116725" y1="11.495140625" x2="108.511525" y2="11.626721875" layer="200"/>
<rectangle x1="94.169425" y1="11.626721875" x2="94.695725" y2="11.758303125" layer="200"/>
<rectangle x1="95.879925" y1="11.626721875" x2="96.406225" y2="11.758303125" layer="200"/>
<rectangle x1="97.195725" y1="11.626721875" x2="97.327325" y2="11.758303125" layer="200"/>
<rectangle x1="100.222025" y1="11.626721875" x2="100.616725" y2="11.758303125" layer="200"/>
<rectangle x1="102.195725" y1="11.626721875" x2="102.458825" y2="11.758303125" layer="200"/>
<rectangle x1="105.485225" y1="11.626721875" x2="105.616725" y2="11.758303125" layer="200"/>
<rectangle x1="106.011525" y1="11.626721875" x2="106.406225" y2="11.758303125" layer="200"/>
<rectangle x1="106.669425" y1="11.626721875" x2="106.932525" y2="11.758303125" layer="200"/>
<rectangle x1="107.327325" y1="11.626721875" x2="107.722025" y2="11.758303125" layer="200"/>
<rectangle x1="108.116725" y1="11.626721875" x2="108.643125" y2="11.758303125" layer="200"/>
<rectangle x1="94.169425" y1="11.758303125" x2="94.564125" y2="11.889878125" layer="200"/>
<rectangle x1="95.748325" y1="11.758303125" x2="96.537825" y2="11.889878125" layer="200"/>
<rectangle x1="97.064125" y1="11.758303125" x2="97.327325" y2="11.889878125" layer="200"/>
<rectangle x1="100.090425" y1="11.758303125" x2="100.485225" y2="11.889878125" layer="200"/>
<rectangle x1="102.327325" y1="11.758303125" x2="102.458825" y2="11.889878125" layer="200"/>
<rectangle x1="105.485225" y1="11.758303125" x2="105.748325" y2="11.889878125" layer="200"/>
<rectangle x1="106.143125" y1="11.758303125" x2="106.406225" y2="11.889878125" layer="200"/>
<rectangle x1="106.669425" y1="11.758303125" x2="107.064125" y2="11.889878125" layer="200"/>
<rectangle x1="107.458825" y1="11.758303125" x2="107.590425" y2="11.889878125" layer="200"/>
<rectangle x1="108.248325" y1="11.758303125" x2="108.643125" y2="11.889878125" layer="200"/>
<rectangle x1="94.169425" y1="11.88988125" x2="94.564125" y2="12.02145" layer="200"/>
<rectangle x1="95.616725" y1="11.88988125" x2="96.011525" y2="12.02145" layer="200"/>
<rectangle x1="96.143125" y1="11.88988125" x2="96.537825" y2="12.02145" layer="200"/>
<rectangle x1="97.064125" y1="11.88988125" x2="97.195725" y2="12.02145" layer="200"/>
<rectangle x1="99.958825" y1="11.88988125" x2="100.485225" y2="12.02145" layer="200"/>
<rectangle x1="102.458825" y1="11.88988125" x2="102.590425" y2="12.02145" layer="200"/>
<rectangle x1="105.616725" y1="11.88988125" x2="105.748325" y2="12.02145" layer="200"/>
<rectangle x1="106.143125" y1="11.88988125" x2="106.406225" y2="12.02145" layer="200"/>
<rectangle x1="106.800925" y1="11.88988125" x2="107.064125" y2="12.02145" layer="200"/>
<rectangle x1="108.248325" y1="11.88988125" x2="108.643125" y2="12.02145" layer="200"/>
<rectangle x1="94.037825" y1="12.021453125" x2="94.564125" y2="12.153028125" layer="200"/>
<rectangle x1="95.616725" y1="12.021453125" x2="95.879925" y2="12.153028125" layer="200"/>
<rectangle x1="96.274625" y1="12.021453125" x2="96.537825" y2="12.153028125" layer="200"/>
<rectangle x1="97.064125" y1="12.021453125" x2="97.195725" y2="12.153028125" layer="200"/>
<rectangle x1="99.827325" y1="12.021453125" x2="100.353625" y2="12.153028125" layer="200"/>
<rectangle x1="102.590425" y1="12.021453125" x2="102.722025" y2="12.153028125" layer="200"/>
<rectangle x1="105.616725" y1="12.021453125" x2="105.748325" y2="12.153028125" layer="200"/>
<rectangle x1="106.274625" y1="12.021453125" x2="106.537825" y2="12.153028125" layer="200"/>
<rectangle x1="108.248325" y1="12.021453125" x2="108.774625" y2="12.153028125" layer="200"/>
<rectangle x1="94.037825" y1="12.15303125" x2="94.564125" y2="12.2846125" layer="200"/>
<rectangle x1="94.958825" y1="12.15303125" x2="95.353625" y2="12.2846125" layer="200"/>
<rectangle x1="95.616725" y1="12.15303125" x2="95.879925" y2="12.2846125" layer="200"/>
<rectangle x1="96.274625" y1="12.15303125" x2="96.537825" y2="12.2846125" layer="200"/>
<rectangle x1="96.932525" y1="12.15303125" x2="97.064125" y2="12.2846125" layer="200"/>
<rectangle x1="99.564125" y1="12.15303125" x2="100.353625" y2="12.2846125" layer="200"/>
<rectangle x1="102.590425" y1="12.15303125" x2="102.853625" y2="12.2846125" layer="200"/>
<rectangle x1="105.748325" y1="12.15303125" x2="105.879925" y2="12.2846125" layer="200"/>
<rectangle x1="106.274625" y1="12.15303125" x2="106.537825" y2="12.2846125" layer="200"/>
<rectangle x1="108.248325" y1="12.15303125" x2="108.774625" y2="12.2846125" layer="200"/>
<rectangle x1="94.037825" y1="12.2846125" x2="94.432525" y2="12.4161875" layer="200"/>
<rectangle x1="94.958825" y1="12.2846125" x2="95.879925" y2="12.4161875" layer="200"/>
<rectangle x1="96.274625" y1="12.2846125" x2="96.537825" y2="12.4161875" layer="200"/>
<rectangle x1="96.932525" y1="12.2846125" x2="97.064125" y2="12.4161875" layer="200"/>
<rectangle x1="99.432525" y1="12.2846125" x2="100.353625" y2="12.4161875" layer="200"/>
<rectangle x1="102.722025" y1="12.2846125" x2="102.853625" y2="12.4161875" layer="200"/>
<rectangle x1="105.748325" y1="12.2846125" x2="105.879925" y2="12.4161875" layer="200"/>
<rectangle x1="108.379925" y1="12.2846125" x2="108.774625" y2="12.4161875" layer="200"/>
<rectangle x1="94.037825" y1="12.416190625" x2="94.432525" y2="12.547771875" layer="200"/>
<rectangle x1="95.090425" y1="12.416190625" x2="96.537825" y2="12.547771875" layer="200"/>
<rectangle x1="96.932525" y1="12.416190625" x2="97.064125" y2="12.547771875" layer="200"/>
<rectangle x1="99.300925" y1="12.416190625" x2="100.222025" y2="12.547771875" layer="200"/>
<rectangle x1="102.853625" y1="12.416190625" x2="102.985225" y2="12.547771875" layer="200"/>
<rectangle x1="105.748325" y1="12.416190625" x2="105.879925" y2="12.547771875" layer="200"/>
<rectangle x1="108.379925" y1="12.416190625" x2="108.774625" y2="12.547771875" layer="200"/>
<rectangle x1="94.037825" y1="12.547771875" x2="94.432525" y2="12.679353125" layer="200"/>
<rectangle x1="95.485225" y1="12.547771875" x2="96.406225" y2="12.679353125" layer="200"/>
<rectangle x1="96.800925" y1="12.547771875" x2="96.932525" y2="12.679353125" layer="200"/>
<rectangle x1="99.432525" y1="12.547771875" x2="100.222025" y2="12.679353125" layer="200"/>
<rectangle x1="102.985225" y1="12.547771875" x2="103.116725" y2="12.679353125" layer="200"/>
<rectangle x1="105.879925" y1="12.547771875" x2="106.011525" y2="12.679353125" layer="200"/>
<rectangle x1="108.379925" y1="12.547771875" x2="108.774625" y2="12.679353125" layer="200"/>
<rectangle x1="94.037825" y1="12.679353125" x2="94.432525" y2="12.810928125" layer="200"/>
<rectangle x1="96.011525" y1="12.679353125" x2="96.406225" y2="12.810928125" layer="200"/>
<rectangle x1="96.800925" y1="12.679353125" x2="96.932525" y2="12.810928125" layer="200"/>
<rectangle x1="99.695725" y1="12.679353125" x2="100.222025" y2="12.810928125" layer="200"/>
<rectangle x1="102.985225" y1="12.679353125" x2="103.116725" y2="12.810928125" layer="200"/>
<rectangle x1="105.879925" y1="12.679353125" x2="106.011525" y2="12.810928125" layer="200"/>
<rectangle x1="108.379925" y1="12.679353125" x2="108.774625" y2="12.810928125" layer="200"/>
<rectangle x1="93.906225" y1="12.81093125" x2="94.432525" y2="12.9425125" layer="200"/>
<rectangle x1="96.800925" y1="12.81093125" x2="96.932525" y2="12.9425125" layer="200"/>
<rectangle x1="99.564125" y1="12.81093125" x2="99.695725" y2="12.9425125" layer="200"/>
<rectangle x1="99.827325" y1="12.81093125" x2="100.090425" y2="12.9425125" layer="200"/>
<rectangle x1="103.116725" y1="12.81093125" x2="103.248325" y2="12.9425125" layer="200"/>
<rectangle x1="105.879925" y1="12.81093125" x2="106.011525" y2="12.9425125" layer="200"/>
<rectangle x1="108.379925" y1="12.81093125" x2="108.906225" y2="12.9425125" layer="200"/>
<rectangle x1="93.906225" y1="12.9425125" x2="94.432525" y2="13.0740875" layer="200"/>
<rectangle x1="96.800925" y1="12.9425125" x2="96.932525" y2="13.0740875" layer="200"/>
<rectangle x1="99.432525" y1="12.9425125" x2="99.564125" y2="13.0740875" layer="200"/>
<rectangle x1="99.958825" y1="12.9425125" x2="100.090425" y2="13.0740875" layer="200"/>
<rectangle x1="103.248325" y1="12.9425125" x2="103.379925" y2="13.0740875" layer="200"/>
<rectangle x1="105.879925" y1="12.9425125" x2="106.011525" y2="13.0740875" layer="200"/>
<rectangle x1="108.379925" y1="12.9425125" x2="108.906225" y2="13.0740875" layer="200"/>
<rectangle x1="93.906225" y1="13.074090625" x2="94.300925" y2="13.205659375" layer="200"/>
<rectangle x1="96.800925" y1="13.074090625" x2="96.932525" y2="13.205659375" layer="200"/>
<rectangle x1="99.300925" y1="13.074090625" x2="99.564125" y2="13.205659375" layer="200"/>
<rectangle x1="103.248325" y1="13.074090625" x2="103.379925" y2="13.205659375" layer="200"/>
<rectangle x1="105.879925" y1="13.074090625" x2="106.011525" y2="13.205659375" layer="200"/>
<rectangle x1="108.511525" y1="13.074090625" x2="108.906225" y2="13.205659375" layer="200"/>
<rectangle x1="93.906225" y1="13.2056625" x2="94.300925" y2="13.3372375" layer="200"/>
<rectangle x1="99.300925" y1="13.2056625" x2="99.432525" y2="13.3372375" layer="200"/>
<rectangle x1="103.379925" y1="13.2056625" x2="103.511525" y2="13.3372375" layer="200"/>
<rectangle x1="108.511525" y1="13.2056625" x2="108.906225" y2="13.3372375" layer="200"/>
<rectangle x1="93.906225" y1="13.337240625" x2="94.300925" y2="13.468821875" layer="200"/>
<rectangle x1="99.169425" y1="13.337240625" x2="99.300925" y2="13.468821875" layer="200"/>
<rectangle x1="103.511525" y1="13.337240625" x2="103.643125" y2="13.468821875" layer="200"/>
<rectangle x1="108.511525" y1="13.337240625" x2="108.906225" y2="13.468821875" layer="200"/>
<rectangle x1="93.906225" y1="13.468821875" x2="94.300925" y2="13.600403125" layer="200"/>
<rectangle x1="96.669425" y1="13.468821875" x2="96.800925" y2="13.600403125" layer="200"/>
<rectangle x1="99.037825" y1="13.468821875" x2="99.169425" y2="13.600403125" layer="200"/>
<rectangle x1="103.511525" y1="13.468821875" x2="103.774625" y2="13.600403125" layer="200"/>
<rectangle x1="106.011525" y1="13.468821875" x2="106.143125" y2="13.600403125" layer="200"/>
<rectangle x1="108.511525" y1="13.468821875" x2="108.906225" y2="13.600403125" layer="200"/>
<rectangle x1="93.906225" y1="13.600403125" x2="94.300925" y2="13.731978125" layer="200"/>
<rectangle x1="96.669425" y1="13.600403125" x2="96.800925" y2="13.731978125" layer="200"/>
<rectangle x1="99.037825" y1="13.600403125" x2="99.169425" y2="13.731978125" layer="200"/>
<rectangle x1="103.643125" y1="13.600403125" x2="103.774625" y2="13.731978125" layer="200"/>
<rectangle x1="106.011525" y1="13.600403125" x2="106.143125" y2="13.731978125" layer="200"/>
<rectangle x1="108.511525" y1="13.600403125" x2="108.906225" y2="13.731978125" layer="200"/>
<rectangle x1="93.906225" y1="13.73198125" x2="94.300925" y2="13.8635625" layer="200"/>
<rectangle x1="96.669425" y1="13.73198125" x2="96.800925" y2="13.8635625" layer="200"/>
<rectangle x1="98.906225" y1="13.73198125" x2="99.037825" y2="13.8635625" layer="200"/>
<rectangle x1="103.774625" y1="13.73198125" x2="103.906225" y2="13.8635625" layer="200"/>
<rectangle x1="106.011525" y1="13.73198125" x2="106.143125" y2="13.8635625" layer="200"/>
<rectangle x1="108.511525" y1="13.73198125" x2="108.906225" y2="13.8635625" layer="200"/>
<rectangle x1="93.906225" y1="13.8635625" x2="94.300925" y2="13.9951375" layer="200"/>
<rectangle x1="96.669425" y1="13.8635625" x2="96.800925" y2="13.9951375" layer="200"/>
<rectangle x1="98.774625" y1="13.8635625" x2="98.906225" y2="13.9951375" layer="200"/>
<rectangle x1="103.906225" y1="13.8635625" x2="104.037825" y2="13.9951375" layer="200"/>
<rectangle x1="106.011525" y1="13.8635625" x2="106.143125" y2="13.9951375" layer="200"/>
<rectangle x1="108.511525" y1="13.8635625" x2="108.906225" y2="13.9951375" layer="200"/>
<rectangle x1="93.906225" y1="13.995140625" x2="94.300925" y2="14.126721875" layer="200"/>
<rectangle x1="96.669425" y1="13.995140625" x2="96.800925" y2="14.126721875" layer="200"/>
<rectangle x1="98.774625" y1="13.995140625" x2="98.906225" y2="14.126721875" layer="200"/>
<rectangle x1="103.906225" y1="13.995140625" x2="104.037825" y2="14.126721875" layer="200"/>
<rectangle x1="108.511525" y1="13.995140625" x2="108.906225" y2="14.126721875" layer="200"/>
<rectangle x1="93.906225" y1="14.126721875" x2="94.300925" y2="14.258303125" layer="200"/>
<rectangle x1="98.643125" y1="14.126721875" x2="98.774625" y2="14.258303125" layer="200"/>
<rectangle x1="104.037825" y1="14.126721875" x2="104.169425" y2="14.258303125" layer="200"/>
<rectangle x1="108.511525" y1="14.126721875" x2="108.906225" y2="14.258303125" layer="200"/>
<rectangle x1="93.906225" y1="14.258303125" x2="94.300925" y2="14.389878125" layer="200"/>
<rectangle x1="96.800925" y1="14.258303125" x2="96.932525" y2="14.389878125" layer="200"/>
<rectangle x1="98.511525" y1="14.258303125" x2="98.643125" y2="14.389878125" layer="200"/>
<rectangle x1="104.169425" y1="14.258303125" x2="104.300925" y2="14.389878125" layer="200"/>
<rectangle x1="105.879925" y1="14.258303125" x2="106.011525" y2="14.389878125" layer="200"/>
<rectangle x1="108.511525" y1="14.258303125" x2="108.906225" y2="14.389878125" layer="200"/>
<rectangle x1="93.906225" y1="14.38988125" x2="94.432525" y2="14.52145" layer="200"/>
<rectangle x1="96.800925" y1="14.38988125" x2="96.932525" y2="14.52145" layer="200"/>
<rectangle x1="98.379925" y1="14.38988125" x2="98.643125" y2="14.52145" layer="200"/>
<rectangle x1="104.169425" y1="14.38988125" x2="104.432525" y2="14.52145" layer="200"/>
<rectangle x1="105.879925" y1="14.38988125" x2="106.011525" y2="14.52145" layer="200"/>
<rectangle x1="108.379925" y1="14.38988125" x2="108.906225" y2="14.52145" layer="200"/>
<rectangle x1="93.906225" y1="14.521453125" x2="94.432525" y2="14.653028125" layer="200"/>
<rectangle x1="96.800925" y1="14.521453125" x2="96.932525" y2="14.653028125" layer="200"/>
<rectangle x1="98.379925" y1="14.521453125" x2="98.511525" y2="14.653028125" layer="200"/>
<rectangle x1="104.300925" y1="14.521453125" x2="104.432525" y2="14.653028125" layer="200"/>
<rectangle x1="105.879925" y1="14.521453125" x2="106.011525" y2="14.653028125" layer="200"/>
<rectangle x1="108.379925" y1="14.521453125" x2="108.906225" y2="14.653028125" layer="200"/>
<rectangle x1="94.037825" y1="14.65303125" x2="94.432525" y2="14.7846125" layer="200"/>
<rectangle x1="96.800925" y1="14.65303125" x2="96.932525" y2="14.7846125" layer="200"/>
<rectangle x1="98.248325" y1="14.65303125" x2="98.379925" y2="14.7846125" layer="200"/>
<rectangle x1="104.432525" y1="14.65303125" x2="104.564125" y2="14.7846125" layer="200"/>
<rectangle x1="105.879925" y1="14.65303125" x2="106.011525" y2="14.7846125" layer="200"/>
<rectangle x1="108.379925" y1="14.65303125" x2="108.774625" y2="14.7846125" layer="200"/>
<rectangle x1="94.037825" y1="14.7846125" x2="94.432525" y2="14.9161875" layer="200"/>
<rectangle x1="96.800925" y1="14.7846125" x2="96.932525" y2="14.9161875" layer="200"/>
<rectangle x1="98.116725" y1="14.7846125" x2="98.248325" y2="14.9161875" layer="200"/>
<rectangle x1="104.432525" y1="14.7846125" x2="104.695725" y2="14.9161875" layer="200"/>
<rectangle x1="105.879925" y1="14.7846125" x2="106.011525" y2="14.9161875" layer="200"/>
<rectangle x1="108.379925" y1="14.7846125" x2="108.774625" y2="14.9161875" layer="200"/>
<rectangle x1="94.037825" y1="14.916190625" x2="94.432525" y2="15.047771875" layer="200"/>
<rectangle x1="96.932525" y1="14.916190625" x2="97.064125" y2="15.047771875" layer="200"/>
<rectangle x1="98.116725" y1="14.916190625" x2="98.248325" y2="15.047771875" layer="200"/>
<rectangle x1="104.564125" y1="14.916190625" x2="104.695725" y2="15.047771875" layer="200"/>
<rectangle x1="105.748325" y1="14.916190625" x2="105.879925" y2="15.047771875" layer="200"/>
<rectangle x1="108.379925" y1="14.916190625" x2="108.774625" y2="15.047771875" layer="200"/>
<rectangle x1="94.037825" y1="15.047771875" x2="94.432525" y2="15.179353125" layer="200"/>
<rectangle x1="96.932525" y1="15.047771875" x2="97.064125" y2="15.179353125" layer="200"/>
<rectangle x1="97.985225" y1="15.047771875" x2="98.116725" y2="15.179353125" layer="200"/>
<rectangle x1="104.695725" y1="15.047771875" x2="104.827325" y2="15.179353125" layer="200"/>
<rectangle x1="105.748325" y1="15.047771875" x2="105.879925" y2="15.179353125" layer="200"/>
<rectangle x1="108.379925" y1="15.047771875" x2="108.774625" y2="15.179353125" layer="200"/>
<rectangle x1="94.037825" y1="15.179353125" x2="94.564125" y2="15.310928125" layer="200"/>
<rectangle x1="96.932525" y1="15.179353125" x2="97.064125" y2="15.310928125" layer="200"/>
<rectangle x1="97.853625" y1="15.179353125" x2="97.985225" y2="15.310928125" layer="200"/>
<rectangle x1="104.827325" y1="15.179353125" x2="104.958825" y2="15.310928125" layer="200"/>
<rectangle x1="105.748325" y1="15.179353125" x2="105.879925" y2="15.310928125" layer="200"/>
<rectangle x1="108.248325" y1="15.179353125" x2="108.774625" y2="15.310928125" layer="200"/>
<rectangle x1="94.037825" y1="15.31093125" x2="94.564125" y2="15.4425125" layer="200"/>
<rectangle x1="97.064125" y1="15.31093125" x2="97.195725" y2="15.4425125" layer="200"/>
<rectangle x1="97.853625" y1="15.31093125" x2="97.985225" y2="15.4425125" layer="200"/>
<rectangle x1="104.827325" y1="15.31093125" x2="104.958825" y2="15.4425125" layer="200"/>
<rectangle x1="105.616725" y1="15.31093125" x2="105.748325" y2="15.4425125" layer="200"/>
<rectangle x1="108.248325" y1="15.31093125" x2="108.774625" y2="15.4425125" layer="200"/>
<rectangle x1="94.169425" y1="15.4425125" x2="94.564125" y2="15.5740875" layer="200"/>
<rectangle x1="97.064125" y1="15.4425125" x2="97.195725" y2="15.5740875" layer="200"/>
<rectangle x1="97.722025" y1="15.4425125" x2="97.853625" y2="15.5740875" layer="200"/>
<rectangle x1="104.958825" y1="15.4425125" x2="105.090425" y2="15.5740875" layer="200"/>
<rectangle x1="105.616725" y1="15.4425125" x2="105.748325" y2="15.5740875" layer="200"/>
<rectangle x1="108.248325" y1="15.4425125" x2="108.643125" y2="15.5740875" layer="200"/>
<rectangle x1="94.169425" y1="15.574090625" x2="94.564125" y2="15.705659375" layer="200"/>
<rectangle x1="97.064125" y1="15.574090625" x2="97.327325" y2="15.705659375" layer="200"/>
<rectangle x1="97.590425" y1="15.574090625" x2="97.722025" y2="15.705659375" layer="200"/>
<rectangle x1="105.090425" y1="15.574090625" x2="105.222025" y2="15.705659375" layer="200"/>
<rectangle x1="105.485225" y1="15.574090625" x2="105.748325" y2="15.705659375" layer="200"/>
<rectangle x1="108.248325" y1="15.574090625" x2="108.643125" y2="15.705659375" layer="200"/>
<rectangle x1="94.169425" y1="15.7056625" x2="94.695725" y2="15.8372375" layer="200"/>
<rectangle x1="97.195725" y1="15.7056625" x2="97.327325" y2="15.8372375" layer="200"/>
<rectangle x1="97.458825" y1="15.7056625" x2="97.722025" y2="15.8372375" layer="200"/>
<rectangle x1="105.090425" y1="15.7056625" x2="105.353625" y2="15.8372375" layer="200"/>
<rectangle x1="105.485225" y1="15.7056625" x2="105.616725" y2="15.8372375" layer="200"/>
<rectangle x1="108.116725" y1="15.7056625" x2="108.643125" y2="15.8372375" layer="200"/>
<rectangle x1="94.300925" y1="15.837240625" x2="94.695725" y2="15.968821875" layer="200"/>
<rectangle x1="97.195725" y1="15.837240625" x2="97.590425" y2="15.968821875" layer="200"/>
<rectangle x1="105.222025" y1="15.837240625" x2="105.616725" y2="15.968821875" layer="200"/>
<rectangle x1="108.116725" y1="15.837240625" x2="108.511525" y2="15.968821875" layer="200"/>
<rectangle x1="94.300925" y1="15.968821875" x2="94.695725" y2="16.100403125" layer="200"/>
<rectangle x1="97.327325" y1="15.968821875" x2="97.458825" y2="16.100403125" layer="200"/>
<rectangle x1="105.353625" y1="15.968821875" x2="105.485225" y2="16.100403125" layer="200"/>
<rectangle x1="108.116725" y1="15.968821875" x2="108.511525" y2="16.100403125" layer="200"/>
<rectangle x1="94.300925" y1="16.100403125" x2="94.827325" y2="16.231978125" layer="200"/>
<rectangle x1="97.458825" y1="16.100403125" x2="97.590425" y2="16.231978125" layer="200"/>
<rectangle x1="105.222025" y1="16.100403125" x2="105.353625" y2="16.231978125" layer="200"/>
<rectangle x1="107.985225" y1="16.100403125" x2="108.511525" y2="16.231978125" layer="200"/>
<rectangle x1="94.300925" y1="16.23198125" x2="94.827325" y2="16.3635625" layer="200"/>
<rectangle x1="97.458825" y1="16.23198125" x2="97.590425" y2="16.3635625" layer="200"/>
<rectangle x1="105.222025" y1="16.23198125" x2="105.353625" y2="16.3635625" layer="200"/>
<rectangle x1="107.985225" y1="16.23198125" x2="108.379925" y2="16.3635625" layer="200"/>
<rectangle x1="94.432525" y1="16.3635625" x2="94.827325" y2="16.4951375" layer="200"/>
<rectangle x1="97.590425" y1="16.3635625" x2="97.722025" y2="16.4951375" layer="200"/>
<rectangle x1="105.090425" y1="16.3635625" x2="105.222025" y2="16.4951375" layer="200"/>
<rectangle x1="107.985225" y1="16.3635625" x2="108.379925" y2="16.4951375" layer="200"/>
<rectangle x1="94.432525" y1="16.495140625" x2="94.958825" y2="16.626721875" layer="200"/>
<rectangle x1="97.722025" y1="16.495140625" x2="97.853625" y2="16.626721875" layer="200"/>
<rectangle x1="104.958825" y1="16.495140625" x2="105.090425" y2="16.626721875" layer="200"/>
<rectangle x1="107.853625" y1="16.495140625" x2="108.379925" y2="16.626721875" layer="200"/>
<rectangle x1="94.564125" y1="16.626721875" x2="94.958825" y2="16.758303125" layer="200"/>
<rectangle x1="97.722025" y1="16.626721875" x2="97.985225" y2="16.758303125" layer="200"/>
<rectangle x1="104.827325" y1="16.626721875" x2="105.090425" y2="16.758303125" layer="200"/>
<rectangle x1="107.853625" y1="16.626721875" x2="108.248325" y2="16.758303125" layer="200"/>
<rectangle x1="94.564125" y1="16.758303125" x2="95.090425" y2="16.889878125" layer="200"/>
<rectangle x1="97.853625" y1="16.758303125" x2="98.116725" y2="16.889878125" layer="200"/>
<rectangle x1="104.695725" y1="16.758303125" x2="104.958825" y2="16.889878125" layer="200"/>
<rectangle x1="107.722025" y1="16.758303125" x2="108.248325" y2="16.889878125" layer="200"/>
<rectangle x1="94.695725" y1="16.88988125" x2="95.090425" y2="17.02145" layer="200"/>
<rectangle x1="97.985225" y1="16.88988125" x2="98.248325" y2="17.02145" layer="200"/>
<rectangle x1="104.564125" y1="16.88988125" x2="104.827325" y2="17.02145" layer="200"/>
<rectangle x1="107.722025" y1="16.88988125" x2="108.116725" y2="17.02145" layer="200"/>
<rectangle x1="94.695725" y1="17.021453125" x2="95.222025" y2="17.153028125" layer="200"/>
<rectangle x1="98.116725" y1="17.021453125" x2="98.379925" y2="17.153028125" layer="200"/>
<rectangle x1="104.432525" y1="17.021453125" x2="104.695725" y2="17.153028125" layer="200"/>
<rectangle x1="107.590425" y1="17.021453125" x2="108.116725" y2="17.153028125" layer="200"/>
<rectangle x1="94.827325" y1="17.15303125" x2="95.222025" y2="17.2846125" layer="200"/>
<rectangle x1="98.248325" y1="17.15303125" x2="98.511525" y2="17.2846125" layer="200"/>
<rectangle x1="104.300925" y1="17.15303125" x2="104.564125" y2="17.2846125" layer="200"/>
<rectangle x1="107.590425" y1="17.15303125" x2="107.985225" y2="17.2846125" layer="200"/>
<rectangle x1="94.827325" y1="17.2846125" x2="95.353625" y2="17.4161875" layer="200"/>
<rectangle x1="98.379925" y1="17.2846125" x2="98.643125" y2="17.4161875" layer="200"/>
<rectangle x1="104.169425" y1="17.2846125" x2="104.432525" y2="17.4161875" layer="200"/>
<rectangle x1="107.458825" y1="17.2846125" x2="107.985225" y2="17.4161875" layer="200"/>
<rectangle x1="94.958825" y1="17.416190625" x2="95.485225" y2="17.547771875" layer="200"/>
<rectangle x1="98.643125" y1="17.416190625" x2="98.774625" y2="17.547771875" layer="200"/>
<rectangle x1="104.037825" y1="17.416190625" x2="104.169425" y2="17.547771875" layer="200"/>
<rectangle x1="107.327325" y1="17.416190625" x2="107.853625" y2="17.547771875" layer="200"/>
<rectangle x1="94.958825" y1="17.547771875" x2="95.485225" y2="17.679353125" layer="200"/>
<rectangle x1="98.774625" y1="17.547771875" x2="99.037825" y2="17.679353125" layer="200"/>
<rectangle x1="103.774625" y1="17.547771875" x2="104.037825" y2="17.679353125" layer="200"/>
<rectangle x1="107.327325" y1="17.547771875" x2="107.853625" y2="17.679353125" layer="200"/>
<rectangle x1="95.090425" y1="17.679353125" x2="95.616725" y2="17.810928125" layer="200"/>
<rectangle x1="99.037825" y1="17.679353125" x2="99.300925" y2="17.810928125" layer="200"/>
<rectangle x1="103.511525" y1="17.679353125" x2="103.774625" y2="17.810928125" layer="200"/>
<rectangle x1="107.195725" y1="17.679353125" x2="107.722025" y2="17.810928125" layer="200"/>
<rectangle x1="95.222025" y1="17.81093125" x2="95.748325" y2="17.9425125" layer="200"/>
<rectangle x1="99.169425" y1="17.81093125" x2="99.564125" y2="17.9425125" layer="200"/>
<rectangle x1="103.248325" y1="17.81093125" x2="103.643125" y2="17.9425125" layer="200"/>
<rectangle x1="107.064125" y1="17.81093125" x2="107.590425" y2="17.9425125" layer="200"/>
<rectangle x1="95.222025" y1="17.9425125" x2="95.748325" y2="18.0740875" layer="200"/>
<rectangle x1="98.643125" y1="17.9425125" x2="98.774625" y2="18.0740875" layer="200"/>
<rectangle x1="99.432525" y1="17.9425125" x2="99.827325" y2="18.0740875" layer="200"/>
<rectangle x1="102.985225" y1="17.9425125" x2="103.379925" y2="18.0740875" layer="200"/>
<rectangle x1="104.037825" y1="17.9425125" x2="104.432525" y2="18.0740875" layer="200"/>
<rectangle x1="107.064125" y1="17.9425125" x2="107.590425" y2="18.0740875" layer="200"/>
<rectangle x1="95.353625" y1="18.074090625" x2="95.879925" y2="18.205659375" layer="200"/>
<rectangle x1="98.379925" y1="18.074090625" x2="99.169425" y2="18.205659375" layer="200"/>
<rectangle x1="99.827325" y1="18.074090625" x2="100.222025" y2="18.205659375" layer="200"/>
<rectangle x1="102.590425" y1="18.074090625" x2="102.985225" y2="18.205659375" layer="200"/>
<rectangle x1="103.774625" y1="18.074090625" x2="104.300925" y2="18.205659375" layer="200"/>
<rectangle x1="106.932525" y1="18.074090625" x2="107.458825" y2="18.205659375" layer="200"/>
<rectangle x1="95.485225" y1="18.2056625" x2="96.011525" y2="18.3372375" layer="200"/>
<rectangle x1="98.248325" y1="18.2056625" x2="99.300925" y2="18.3372375" layer="200"/>
<rectangle x1="100.222025" y1="18.2056625" x2="100.879925" y2="18.3372375" layer="200"/>
<rectangle x1="101.932525" y1="18.2056625" x2="102.590425" y2="18.3372375" layer="200"/>
<rectangle x1="103.511525" y1="18.2056625" x2="104.037825" y2="18.3372375" layer="200"/>
<rectangle x1="106.800925" y1="18.2056625" x2="107.327325" y2="18.3372375" layer="200"/>
<rectangle x1="95.485225" y1="18.337240625" x2="96.143125" y2="18.468821875" layer="200"/>
<rectangle x1="99.037825" y1="18.337240625" x2="99.432525" y2="18.468821875" layer="200"/>
<rectangle x1="101.143125" y1="18.337240625" x2="101.669425" y2="18.468821875" layer="200"/>
<rectangle x1="103.379925" y1="18.337240625" x2="103.906225" y2="18.468821875" layer="200"/>
<rectangle x1="106.669425" y1="18.337240625" x2="107.195725" y2="18.468821875" layer="200"/>
<rectangle x1="95.616725" y1="18.468821875" x2="96.274625" y2="18.600403125" layer="200"/>
<rectangle x1="99.037825" y1="18.468821875" x2="99.432525" y2="18.600403125" layer="200"/>
<rectangle x1="103.379925" y1="18.468821875" x2="103.774625" y2="18.600403125" layer="200"/>
<rectangle x1="104.432525" y1="18.468821875" x2="104.564125" y2="18.600403125" layer="200"/>
<rectangle x1="106.537825" y1="18.468821875" x2="107.195725" y2="18.600403125" layer="200"/>
<rectangle x1="95.748325" y1="18.600403125" x2="96.406225" y2="18.731978125" layer="200"/>
<rectangle x1="98.116725" y1="18.600403125" x2="98.774625" y2="18.731978125" layer="200"/>
<rectangle x1="99.037825" y1="18.600403125" x2="99.432525" y2="18.731978125" layer="200"/>
<rectangle x1="100.090425" y1="18.600403125" x2="100.353625" y2="18.731978125" layer="200"/>
<rectangle x1="102.064125" y1="18.600403125" x2="102.722025" y2="18.731978125" layer="200"/>
<rectangle x1="103.379925" y1="18.600403125" x2="103.774625" y2="18.731978125" layer="200"/>
<rectangle x1="104.169425" y1="18.600403125" x2="104.695725" y2="18.731978125" layer="200"/>
<rectangle x1="106.406225" y1="18.600403125" x2="107.064125" y2="18.731978125" layer="200"/>
<rectangle x1="95.879925" y1="18.73198125" x2="96.537825" y2="18.8635625" layer="200"/>
<rectangle x1="97.985225" y1="18.73198125" x2="99.300925" y2="18.8635625" layer="200"/>
<rectangle x1="100.090425" y1="18.73198125" x2="100.353625" y2="18.8635625" layer="200"/>
<rectangle x1="101.800925" y1="18.73198125" x2="102.853625" y2="18.8635625" layer="200"/>
<rectangle x1="103.511525" y1="18.73198125" x2="104.432525" y2="18.8635625" layer="200"/>
<rectangle x1="106.274625" y1="18.73198125" x2="106.932525" y2="18.8635625" layer="200"/>
<rectangle x1="96.011525" y1="18.8635625" x2="96.669425" y2="18.9951375" layer="200"/>
<rectangle x1="97.985225" y1="18.8635625" x2="98.379925" y2="18.9951375" layer="200"/>
<rectangle x1="98.511525" y1="18.8635625" x2="99.169425" y2="18.9951375" layer="200"/>
<rectangle x1="99.958825" y1="18.8635625" x2="100.353625" y2="18.9951375" layer="200"/>
<rectangle x1="101.800925" y1="18.8635625" x2="102.195725" y2="18.9951375" layer="200"/>
<rectangle x1="102.458825" y1="18.8635625" x2="102.853625" y2="18.9951375" layer="200"/>
<rectangle x1="103.643125" y1="18.8635625" x2="104.169425" y2="18.9951375" layer="200"/>
<rectangle x1="106.143125" y1="18.8635625" x2="106.800925" y2="18.9951375" layer="200"/>
<rectangle x1="96.143125" y1="18.995140625" x2="96.800925" y2="19.126721875" layer="200"/>
<rectangle x1="97.985225" y1="18.995140625" x2="98.248325" y2="19.126721875" layer="200"/>
<rectangle x1="99.958825" y1="18.995140625" x2="100.353625" y2="19.126721875" layer="200"/>
<rectangle x1="101.669425" y1="18.995140625" x2="102.064125" y2="19.126721875" layer="200"/>
<rectangle x1="102.590425" y1="18.995140625" x2="102.853625" y2="19.126721875" layer="200"/>
<rectangle x1="103.643125" y1="18.995140625" x2="104.037825" y2="19.126721875" layer="200"/>
<rectangle x1="104.564125" y1="18.995140625" x2="104.958825" y2="19.126721875" layer="200"/>
<rectangle x1="106.011525" y1="18.995140625" x2="106.669425" y2="19.126721875" layer="200"/>
<rectangle x1="96.274625" y1="19.126721875" x2="96.932525" y2="19.258303125" layer="200"/>
<rectangle x1="97.985225" y1="19.126721875" x2="98.379925" y2="19.258303125" layer="200"/>
<rectangle x1="98.643125" y1="19.126721875" x2="98.906225" y2="19.258303125" layer="200"/>
<rectangle x1="99.958825" y1="19.126721875" x2="100.485225" y2="19.258303125" layer="200"/>
<rectangle x1="101.669425" y1="19.126721875" x2="101.932525" y2="19.258303125" layer="200"/>
<rectangle x1="102.458825" y1="19.126721875" x2="102.853625" y2="19.258303125" layer="200"/>
<rectangle x1="103.774625" y1="19.126721875" x2="104.037825" y2="19.258303125" layer="200"/>
<rectangle x1="104.432525" y1="19.126721875" x2="104.958825" y2="19.258303125" layer="200"/>
<rectangle x1="105.879925" y1="19.126721875" x2="106.537825" y2="19.258303125" layer="200"/>
<rectangle x1="96.406225" y1="19.258303125" x2="97.064125" y2="19.389878125" layer="200"/>
<rectangle x1="98.116725" y1="19.258303125" x2="99.037825" y2="19.389878125" layer="200"/>
<rectangle x1="99.958825" y1="19.258303125" x2="100.748325" y2="19.389878125" layer="200"/>
<rectangle x1="102.195725" y1="19.258303125" x2="102.853625" y2="19.389878125" layer="200"/>
<rectangle x1="103.774625" y1="19.258303125" x2="104.695725" y2="19.389878125" layer="200"/>
<rectangle x1="105.748325" y1="19.258303125" x2="106.406225" y2="19.389878125" layer="200"/>
<rectangle x1="96.537825" y1="19.38988125" x2="97.327325" y2="19.52145" layer="200"/>
<rectangle x1="98.379925" y1="19.38988125" x2="98.906225" y2="19.52145" layer="200"/>
<rectangle x1="99.827325" y1="19.38988125" x2="100.222025" y2="19.52145" layer="200"/>
<rectangle x1="100.353625" y1="19.38988125" x2="100.879925" y2="19.52145" layer="200"/>
<rectangle x1="101.932525" y1="19.38988125" x2="102.722025" y2="19.52145" layer="200"/>
<rectangle x1="103.906225" y1="19.38988125" x2="104.432525" y2="19.52145" layer="200"/>
<rectangle x1="105.485225" y1="19.38988125" x2="106.274625" y2="19.52145" layer="200"/>
<rectangle x1="96.669425" y1="19.521453125" x2="97.458825" y2="19.653028125" layer="200"/>
<rectangle x1="99.827325" y1="19.521453125" x2="100.222025" y2="19.653028125" layer="200"/>
<rectangle x1="100.616725" y1="19.521453125" x2="101.011525" y2="19.653028125" layer="200"/>
<rectangle x1="101.932525" y1="19.521453125" x2="102.327325" y2="19.653028125" layer="200"/>
<rectangle x1="103.906225" y1="19.521453125" x2="104.169425" y2="19.653028125" layer="200"/>
<rectangle x1="105.353625" y1="19.521453125" x2="106.011525" y2="19.653028125" layer="200"/>
<rectangle x1="96.932525" y1="19.65303125" x2="97.722025" y2="19.7846125" layer="200"/>
<rectangle x1="99.827325" y1="19.65303125" x2="100.090425" y2="19.7846125" layer="200"/>
<rectangle x1="100.616725" y1="19.65303125" x2="101.011525" y2="19.7846125" layer="200"/>
<rectangle x1="101.932525" y1="19.65303125" x2="102.195725" y2="19.7846125" layer="200"/>
<rectangle x1="102.590425" y1="19.65303125" x2="102.985225" y2="19.7846125" layer="200"/>
<rectangle x1="105.090425" y1="19.65303125" x2="105.879925" y2="19.7846125" layer="200"/>
<rectangle x1="97.064125" y1="19.7846125" x2="97.853625" y2="19.9161875" layer="200"/>
<rectangle x1="99.827325" y1="19.7846125" x2="100.353625" y2="19.9161875" layer="200"/>
<rectangle x1="100.485225" y1="19.7846125" x2="101.011525" y2="19.9161875" layer="200"/>
<rectangle x1="101.932525" y1="19.7846125" x2="102.195725" y2="19.9161875" layer="200"/>
<rectangle x1="102.458825" y1="19.7846125" x2="102.985225" y2="19.9161875" layer="200"/>
<rectangle x1="104.958825" y1="19.7846125" x2="105.748325" y2="19.9161875" layer="200"/>
<rectangle x1="97.327325" y1="19.916190625" x2="98.116725" y2="20.047771875" layer="200"/>
<rectangle x1="99.827325" y1="19.916190625" x2="100.879925" y2="20.047771875" layer="200"/>
<rectangle x1="101.932525" y1="19.916190625" x2="102.853625" y2="20.047771875" layer="200"/>
<rectangle x1="104.695725" y1="19.916190625" x2="105.485225" y2="20.047771875" layer="200"/>
<rectangle x1="97.458825" y1="20.047771875" x2="98.379925" y2="20.179353125" layer="200"/>
<rectangle x1="100.222025" y1="20.047771875" x2="100.748325" y2="20.179353125" layer="200"/>
<rectangle x1="102.064125" y1="20.047771875" x2="102.590425" y2="20.179353125" layer="200"/>
<rectangle x1="104.432525" y1="20.047771875" x2="105.353625" y2="20.179353125" layer="200"/>
<rectangle x1="97.722025" y1="20.179353125" x2="98.643125" y2="20.310928125" layer="200"/>
<rectangle x1="102.327325" y1="20.179353125" x2="102.722025" y2="20.310928125" layer="200"/>
<rectangle x1="104.169425" y1="20.179353125" x2="105.090425" y2="20.310928125" layer="200"/>
<rectangle x1="97.985225" y1="20.31093125" x2="99.037825" y2="20.4425125" layer="200"/>
<rectangle x1="102.327325" y1="20.31093125" x2="102.853625" y2="20.4425125" layer="200"/>
<rectangle x1="103.774625" y1="20.31093125" x2="104.827325" y2="20.4425125" layer="200"/>
<rectangle x1="98.248325" y1="20.4425125" x2="99.432525" y2="20.5740875" layer="200"/>
<rectangle x1="102.195725" y1="20.4425125" x2="102.458825" y2="20.5740875" layer="200"/>
<rectangle x1="103.379925" y1="20.4425125" x2="104.564125" y2="20.5740875" layer="200"/>
<rectangle x1="98.511525" y1="20.574090625" x2="99.958825" y2="20.705659375" layer="200"/>
<rectangle x1="102.853625" y1="20.574090625" x2="104.300925" y2="20.705659375" layer="200"/>
<rectangle x1="98.774625" y1="20.7056625" x2="100.879925" y2="20.8372375" layer="200"/>
<rectangle x1="101.932525" y1="20.7056625" x2="103.906225" y2="20.8372375" layer="200"/>
<rectangle x1="99.300925" y1="20.837240625" x2="103.511525" y2="20.968821875" layer="200"/>
<rectangle x1="99.695725" y1="20.968821875" x2="103.116725" y2="21.100403125" layer="200"/>
<rectangle x1="100.485225" y1="21.100403125" x2="102.327325" y2="21.231978125" layer="200"/>
<text x="111.557815625" y="10.37793125" size="2.54" layer="200" ratio="20">SPŠE a VOŠ
Pardubice</text>
<text x="8.89" y="121.92" size="2.54" layer="94">Tristate buffer connector - big</text>
</plain>
<instances>
<instance part="TO_3STATE" gate="A" x="86.36" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="78.994" y="88.519" size="1.778" layer="95"/>
<attribute name="VALUE" x="96.52" y="80.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IN" gate="A" x="106.68" y="71.12"/>
<instance part="OUT" gate="A" x="68.58" y="58.42" rot="R180"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
</instances>
<busses>
</busses>
<nets>
<net name="C" class="0">
<segment>
<wire x1="86.36" y1="83.82" x2="86.36" y2="71.12" width="0.1524" layer="91"/>
<wire x1="86.36" y1="71.12" x2="104.14" y2="71.12" width="0.1524" layer="91"/>
<wire x1="86.36" y1="71.12" x2="86.36" y2="58.42" width="0.1524" layer="91"/>
<junction x="86.36" y="71.12"/>
<wire x1="86.36" y1="58.42" x2="71.12" y2="58.42" width="0.1524" layer="91"/>
<label x="93.98" y="71.12" size="1.778" layer="95"/>
<pinref part="TO_3STATE" gate="A" pin="3"/>
<pinref part="IN" gate="A" pin="3"/>
<pinref part="OUT" gate="A" pin="3"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="71.12" y1="60.96" x2="88.9" y2="60.96" width="0.1524" layer="91"/>
<wire x1="88.9" y1="60.96" x2="88.9" y2="68.58" width="0.1524" layer="91"/>
<wire x1="88.9" y1="68.58" x2="88.9" y2="83.82" width="0.1524" layer="91"/>
<wire x1="104.14" y1="68.58" x2="88.9" y2="68.58" width="0.1524" layer="91"/>
<junction x="88.9" y="68.58"/>
<label x="93.98" y="68.58" size="1.778" layer="95"/>
<pinref part="TO_3STATE" gate="A" pin="4"/>
<pinref part="IN" gate="A" pin="4"/>
<pinref part="OUT" gate="A" pin="4"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<wire x1="71.12" y1="63.5" x2="91.44" y2="63.5" width="0.1524" layer="91"/>
<wire x1="91.44" y1="63.5" x2="91.44" y2="66.04" width="0.1524" layer="91"/>
<wire x1="91.44" y1="66.04" x2="91.44" y2="83.82" width="0.1524" layer="91"/>
<wire x1="104.14" y1="66.04" x2="91.44" y2="66.04" width="0.1524" layer="91"/>
<junction x="91.44" y="66.04"/>
<label x="93.98" y="66.04" size="1.778" layer="95"/>
<pinref part="TO_3STATE" gate="A" pin="5"/>
<pinref part="IN" gate="A" pin="5"/>
<pinref part="OUT" gate="A" pin="5"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="OUT" gate="A" pin="1"/>
<wire x1="71.12" y1="53.34" x2="81.28" y2="53.34" width="0.1524" layer="91"/>
<pinref part="TO_3STATE" gate="A" pin="1"/>
<wire x1="81.28" y1="53.34" x2="81.28" y2="76.2" width="0.1524" layer="91"/>
<pinref part="IN" gate="A" pin="1"/>
<wire x1="81.28" y1="76.2" x2="81.28" y2="83.82" width="0.1524" layer="91"/>
<wire x1="104.14" y1="76.2" x2="81.28" y2="76.2" width="0.1524" layer="91"/>
<junction x="81.28" y="76.2"/>
<label x="93.98" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
