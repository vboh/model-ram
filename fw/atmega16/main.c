/*
ATmega16
 */

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "RS232.h"

#define DEBUG_LED_PIN 4
#define DEBUG_LED_DDR DDRB
#define DEBUG_LED_PORT PORTB

volatile uint8_t instruction = 0;

/**
    Toggle DEBUG LED
*/
void debug_led() {
    DEBUG_LED_PORT ^= 1 << DEBUG_LED_PIN;
};

/**
    CLK - slave
*/
void init_clk_slave() {
    cli();

    MCUCR |= (1 << ISC00);
    GICR |= (1 << INT0);

    sei();
}

/**CHIP SELECT*/
#define CHIP_SELECT_PIN 3
#define CHIP_SELECT_PIN_REG PIND

/**CONTROL*/
#define CONTROL_PIN 6
#define CONTROL_PIN_REG PIND

/**ADDRESS lines*/
uint8_t adr_read() {
                //  76543210
    return (PIND & 0b00110000) >> 4;
}

/**DATA lines*/
void data_links_readable() {
    DDRA = 0x00;
}

void data_links_writeable() {
    DDRA = 0xFF;
}

void data_links_write(uint8_t value) {
    PINA = value;
}

uint8_t data_links_read() {
    return PINA;
}

void data_links_reset() {
    data_links_write(0);
    data_links_readable();
}

/**MEMORY SLECT ADDRESS*/
#define MEMORY_SELECT_ADDRESS_CLEAR 4

void init_memory_select_address() {
    DDRB |= 0b00001111;
}

void memory_select_address(uint8_t adr) {
    PORTB &= ~(0b00001111);
    switch (adr) {
        case 0 : {
            PORTB |= 0b00000001;
            break;
        }

        case 1 : {
            PORTB |= 0b00000010;
            break;
        }

        case 2 : {
            PORTB |= 0b00000100;
            break;
        }

        case 3 : {
            PORTB |= 0b00001000;
            break;
        }

        case 4 : {
            PORTB &= ~(0b00001111);
            break;
        }
    }
}

/**MEMORY OPERATIONS*/
void memory_writeable() {
    DDRC = 0xFF;
}

void memory_readable() {
    DDRC = 0x00;
}

void memory_write(uint8_t value) {
    PORTC = value;
}

uint8_t memory_read() {
    return PINC;
}

void memory_reset() {
    memory_readable();
    memory_select_address(MEMORY_SELECT_ADDRESS_CLEAR);
    memory_write(0);
}

/**CLK SLAVE ISR*/
ISR(INT0_vect) {
    if(PIND & (1 << 2)) { //CLK HIGH

        switch(instruction) {
            case 2 : {
                //memory_reset();
                instruction = 0;
                break;
            }
        }

        if(CHIP_SELECT_PIN_REG & (1 << CHIP_SELECT_PIN)) {
            //DEBUG_LED_PORT |= 1 << DEBUG_LED_PIN;
            if(CONTROL_PIN_REG & (1 << CONTROL_PIN)) {
                /**DATA OUTCOME*/
            } else {
                /**DATA INCOME*/
                PORTB &= 0b11110000;
                PORTB |= (1 << adr_read());
                data_links_readable();
                DDRC |= 0xFF;
                PORTC = data_links_read();
                instruction = 1;
            }
        }
DEBUG_LED_PORT &= ~(1 << DEBUG_LED_PIN);
    } else { //CLK LOW
        //DEBUG_LED_PORT &= ~(1 << DEBUG_LED_PIN);
        switch(instruction) {
            case 1 : {
                PORTB &= 0b11110000;
                PORTC = 0x00;
                PINC = 0;
                DDRC &= 0x00;
                instruction = 0;
                send_buffer_write(254);
                send_buffer_write(PINC);
                break;
            }
        }
    }
}

/**REFRESH*/

volatile uint8_t word = 0, data, refresh_counter = 0;

void init_refresh() {
    cli();

    TIMSK |= (1 << TOIE0);

    TCCR0 &= ~(1 << CS01);
    TCCR0 |= (1 << CS02)
            |(1 << CS00)
            ;

    sei();
}

ISR(TIMER0_OVF_vect) {
    refresh_counter++;
    if(refresh_counter > 31) {
        DEBUG_LED_PORT ^= 1 << DEBUG_LED_PIN;

        word++;
        if(word == 4) {
            word = 0;
        }
        refresh_counter = 0;
    }
}

/**CHIP SELECT SLAVE*/
/*void init_CS_slave() {
    cli();

    MCUCR |= (1 << ISC10);
    GICR |= (1 << INT1);

    sei();
}

ISR(INT1_vect) {
    if(PIND & (1 << 3)) {

    } else {

    }
}*/

/**
Init ALL
*/
void init() {
    //Init debug LED
    DEBUG_LED_DDR |= 1 << DEBUG_LED_PIN;

    //Init CLK_SLAVE
    init_clk_slave();

    //Init CHIP SELECT SLAVE
    //init_CS_slave();

    //Init memory addressation
    init_memory_select_address();

    init_refresh();

    init_rs232();
}

int main(void) {
    init();
    // Insert code

    //debug_led();
/*
    _delay_ms(1000);

    memory_select_address(0);
    memory_writeable();
    memory_write(0b01010101);
    _delay_ms(10);
    memory_reset();

    memory_select_address(1);
    memory_writeable();
    memory_write(0b10101010);
    _delay_ms(10);
    memory_reset();

    memory_select_address(2);
    memory_writeable();
    memory_write(0b11001100);
    _delay_ms(10);
    memory_reset();

    memory_select_address(3);
    memory_writeable();
    memory_write(0b00110011);
    _delay_ms(10);
    memory_reset();
*/


    while(1) {
        /*_delay_ms(500);
        DEBUG_LED_PORT ^= 1 << DEBUG_LED_PIN;*/
    };


    return 0;
}
