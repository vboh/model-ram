#include <avr/io.h>
#include <avr/interrupt.h>

#define BAUD_RATE 9600
#define V_UBRR (((F_CPU / (BAUD_RATE * 16UL))) - 1)

/**
    Function definitions
*/

uint8_t income_buffer_write(uint8_t data);
uint8_t income_buffer_read(void);
uint8_t income_data();
uint8_t send_buffer_read(void);

/**
    Variable definitions
*/

//RX
volatile uint8_t income_buffer[32];
volatile uint8_t i_b_read_pos = 0,
                 i_b_write_pos = 0,
                 i_b_data_len = 0;

//TX
volatile uint8_t send_buffer[32];
volatile uint8_t s_b_read_pos = 0,
                 s_b_write_pos = 0,
                 s_b_data_len = 0,
                 sending = 0;

/**
    HW specific functions
*/
#if defined (__AVR_ATmega16__)

#define UDR_RS232 UDR
#define TX_COMPLETE_BIT TXC
#define TX_COMPL_USART_CNTR_REG UCSRA

    ISR(USART_RXC_vect) {
        income_buffer_write(UDR_RS232);
    }

    ISR(USART_TXC_vect) {
        //Clear TX Complete bit
        TX_COMPL_USART_CNTR_REG &= ~(1 << TX_COMPLETE_BIT);
        if(s_b_data_len > 0) {
            UDR_RS232 = send_buffer_read();
        } else {
            sending = 0;
        }
    }

    void init_rs232() {
        cli();

        UBRRH = (uint8_t) (V_UBRR >> 8) & ~(1 << URSEL);
        UBRRL = (uint8_t) V_UBRR;
        UCSRB |=  (1 << RXCIE) // Enable RX interrupt
                  | (1 << TXCIE) // Enable TX interrupt
                  | (1 << RXEN) // Enable RX
                  | (1 << TXEN) // Enable TX
                  ;

        UCSRC |=   (1 << URSEL)
                  | (0 << USBS)
                  | (1 << UCSZ1)
                  | (1 << UCSZ0);

        sei();
    }


#endif // defined (__AVR_ATmega8__)
#if defined (__AVR_ATmega640P__)

#define UDR_RS232 UDR0
#define TX_COMPLETE_BIT TXC0
#define TX_COMPL_USART_CNTR_REG UCSR0A

    ISR(USART0_TX_vect) {
        income_buffer_write(UDR_RS232);
    }

    ISR(USART0_RX_vect) {
        //Clear TX Complete bit
        TX_COMPL_USART_CNTR_REG &= ~(1 << TX_COMPLETE_BIT);
        if(s_b_data_len > 0) {
            UDR_RS232 = send_buffer_read();
        } else {
            sending = 0;
        }
    }

    void init_rs232() {
        cli();
        UBRR0H = (uint8_t) (V_UBRR >> 8);
        UBRR0L = (uint8_t) V_UBRR;
        UCSR0B |=  (1 << RXCIE0) // Enable RX interrupt
                  | (1 << TXCIE0) // Enable TX interrupt
                  | (1 << RXEN0) // Enable RX
                  | (1 << TXEN0) // Enable TX
                  ;

        UCSR0C |=   (0 << USBS0)
                  | (1 << UCSZ01)
                  | (1 << UCSZ00);
        sei();
    }

#endif //defined (__AVR_ATmega640P__)


 // RX data buffer




uint8_t income_buffer_write(uint8_t data) {
    if(i_b_data_len <= 32) {
        income_buffer[i_b_write_pos] = data;
        i_b_data_len++;
        i_b_write_pos = (i_b_write_pos < 31) ? i_b_write_pos + 1 : 0;
        return 1;
    }
    return 0;
}

uint8_t income_buffer_read() {
    if(i_b_data_len > 0) {
        uint8_t data = income_buffer[i_b_read_pos];
        i_b_data_len--;
        i_b_read_pos = (i_b_read_pos < 31) ? i_b_read_pos + 1 : 0;
        return data;
    }
    return 0;
}

uint8_t income_data() {
    return i_b_data_len;
}

// TX data buffer



uint8_t send_buffer_read() {
    if(s_b_data_len > 0) {
        uint8_t data = send_buffer[s_b_read_pos];
        s_b_data_len--;
        s_b_read_pos = (s_b_read_pos < 31) ? s_b_read_pos + 1 : 0;
        return data;
    }
    return 0;
}

uint8_t send_buffer_write(uint8_t data) {
    if(s_b_data_len <= 32) {
        send_buffer[s_b_write_pos] = data;
        s_b_data_len++;
        s_b_write_pos = (s_b_write_pos < 31) ? s_b_write_pos + 1 : 0;
        if(!sending) {
            sending = 1;
            UDR_RS232 = send_buffer_read();
        }
        return 1;
    }
    return 0;
}
