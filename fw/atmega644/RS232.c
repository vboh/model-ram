/*
Copyright (C) 2017  Viktor Bohunek

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, go to:
https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/
#include <avr/io.h>
#include <avr/interrupt.h>

/**
    Priprava nastaveni BAUD rate prenosu - vypocet frekvence provede preprocesor kompilatoru,
    setri se tak prostredky mikrokontroleru
*/
#define BAUD_RATE 9600
#define V_UBRR (((F_CPU / (BAUD_RATE * 16UL))) - 1)

/**
    Definice funkci
*/
uint8_t income_buffer_write(uint8_t data);
uint8_t income_buffer_read(void);
uint8_t income_data();
uint8_t send_buffer_read(void);

/**
    Definice promennych
*/
//RX
volatile uint8_t income_buffer[32];
volatile uint8_t i_b_read_pos = 0,
                 i_b_write_pos = 0,
                 i_b_data_len = 0;

//TX
volatile uint8_t send_buffer[32];
volatile uint8_t s_b_read_pos = 0,
                 s_b_write_pos = 0,
                 s_b_data_len = 0,
                 sending = 0;

/**
    HW specificke funkce
*/

///Funkce pro ATmega644P
#define UDR_RS232 UDR0 ///Registr s daty USART
#define TX_COMPLETE_BIT TXC0 ///Alias pro bit, signalizujici prijeti dat
#define TX_COMPL_USART_CNTR_REG UCSR0A ///Kontrolní registr, kde se vyskytuje predchozi bit

/**
    Funkce reaguje na vektor preruseni, kterz je vzvolan pri prijeti BYTU

    Prichozi BYTE je ihned vlozen do vstupni fronty
*/
    ISR(USART0_RX_vect) {
        income_buffer_write(UDR_RS232);
    }

/**
    Funkce reaguje na presruseni vyvolane odeslanim BYTU

    Po deslani BYTU je zkontrolovan pocet BYTU k odeslani a pokud ve vystupni
    fronte nejaky je, je ihned vlozen do registu UDR k odeslani
    Pokud nejsou zadne BYTY ve vystupni fronte zmeni se stav sending na FALSE
*/
    ISR(USART0_TX_vect) {
        TX_COMPL_USART_CNTR_REG &= ~(1 << TX_COMPLETE_BIT); //Vynulovani TXC bitu, jinak je neustale volano preruseni
        if(s_b_data_len > 0) {
            UDR_RS232 = send_buffer_read();
        } else {
            sending = 0;
        }
    }

/**
    Inicializace komponenty USART

    UBBR - 16 bitovy regist - nastaveni BAUDrate
    UCSRx - kontrolni registry USART - nastaveni delky dat, parity, start a stop bitu,
        povoleni preruseni
*/
    void init_rs232() {
        cli();
        UBRR0H = (uint8_t) (V_UBRR >> 8);
        UBRR0L = (uint8_t) V_UBRR;
        UCSR0B |=  (1 << RXCIE0) // Povoleni presruseni pri prijeti
                  | (1 << TXCIE0) // Povoleni presruseni pri odeslani
                  | (1 << RXEN0) // Povoleni prijmace
                  | (1 << TXEN0) // Povoleni vysilace
                  ;

        UCSR0C |=   (0 << USBS0)
                  | (1 << UCSZ01)
                  | (1 << UCSZ00);
        sei();
    }


///Fronta prichozich dat

/**
    Funkce vklada data do pole, ktere diky kombinaci nekolika promeny simuluje FIFO
    Momentalne je nastavena pevna delka 32 BYTU
*/
uint8_t income_buffer_write(uint8_t data) {
    if(i_b_data_len <= 32) { //Aby nemohla fronta pretect
        income_buffer[i_b_write_pos] = data;
        i_b_data_len++; //Pocet dat ve fronte
        i_b_write_pos = (i_b_write_pos < 31) ? i_b_write_pos + 1 : 0;  //Do pole se zapisuje cyklycky
        return 1;
    }
    return 0;
}

/**
    Funkce cte z FIFO data
*/
uint8_t income_buffer_read() {
    if(i_b_data_len > 0) { //Pokud nejsou ve fronte data, vraci se 0
        uint8_t data = income_buffer[i_b_read_pos];
        i_b_data_len--; //Pocet dat ve fronte
        i_b_read_pos = (i_b_read_pos < 31) ? i_b_read_pos + 1 : 0; //Z pole se cte cyklycky
        return data;
    }
    return 0;
}

/**
    Funcke pro overeni pritomnosti dat ve fronte
*/
uint8_t income_data() {
    return i_b_data_len;
}

///Fronta odchozich dat

/**
    Funkce cte z FIFO data
*/
uint8_t send_buffer_read() {
    if(s_b_data_len > 0) { //Pokud nejsou ve fronte data, vraci se 0
        uint8_t data = send_buffer[s_b_read_pos];
        s_b_data_len--; //Pocet dat ve fronte
        s_b_read_pos = (s_b_read_pos < 31) ? s_b_read_pos + 1 : 0; //Z pole se cte cyklycky
        return data;
    }
    return 0;
}

/**
    Funkce vklada data do pole, ktere diky kombinaci nekolika promeny simuluje FIFO
    Momentalne je nastavena pevna delka 32 BYTU
*/
uint8_t send_buffer_write(uint8_t data) {
    if(s_b_data_len <= 32) { //Aby nemohla fronta pretect
        send_buffer[s_b_write_pos] = data;
        s_b_data_len++; //Pocet dat ve fronte
        s_b_write_pos = (s_b_write_pos < 31) ? s_b_write_pos + 1 : 0; //Z pole se cte cyklycky
        if(!sending) { //Pokud se neposilaji data, zahaji se prenos
            sending = 1; //Zmena stavu - prave se odesila, kontrolu prebira obsluzna rutina preruseni
            UDR_RS232 = send_buffer_read(); //data k odeslani
        }
        return 1;
    }
    return 0;
}
