/*
Copyright (C) 2017  Viktor Bohunek

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, go to:
https://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Atmega644P
*/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "RS232.h"

#define DEBUG_LED_PIN 4
#define DEBUG_LED_DDR DDRB
#define DEBUG_LED_PORT PORTB

volatile uint8_t instruction = 0;

/**
    Debug LED
    soubor definic a funkce, ktery umoznoval blikat diodou a pomahat tak zjistit stav programu

    VE FINALNI VERZI NEIMPLEMENTOANO
*/
void debug_led() {
    DEBUG_LED_PORT ^= 1 << DEBUG_LED_PIN;
};

/**
    CLK

    Pomoci obsluzne rutiny signalu externiho preruseni INT0 je dosazeno synchronizace
    operaci radice s procesorem (ATmega8)
*/
void init_clk_slave() {
    cli();

    EICRA |= (1 << ISC00); //Povoleni preruseni INT0 pri kazde zmene signalu
    EIMSK |= (1 << INT0);

    sei();
}

/**
    * Chip Select
    * Control
*/

///Definice pinu a portu Chip Selectu
#define CHIP_SELECT_PIN 3
#define CHIP_SELECT_PIN_REG PIND

///Definici pinu a portu Control
#define CONTROL_PIN 6
#define CONTROL_PIN_REG PIND

/**
    Adresacni vodice
*/

uint8_t adr_read() {
    return (PIND & 0b00110000) >> 4;
}

/**DATA lines*/
void data_links_readable() {
    DDRA = 0x00;
}

void data_links_writeable() {
    DDRA = 0xFF;
}

void data_links_write(uint8_t value) {
    PINA = value;
}

uint8_t data_links_read() {
    return PINA;
}

void data_links_reset() {
    data_links_write(0);
    data_links_readable();
}

/**
    Operace s pametovym polem
*/
///inicializace vyberu radku
void init_memory_select_address() {
    DDRB |= 0b00001111;
}

///prepnuti sloupcu pro zapis do pole
void memory_writeable() {
    DDRC = 0xFF;
}

///prepnuti sloupcu pro cteni z pole
void memory_readable() {
    DDRC = 0x00;
}

///zapsani dat na sloupce
void memory_write(uint8_t value) {
    PORTC = value;
}

///cteni dat ze sloupcu
uint8_t memory_read() {
    return PINC;
}

void memory_reset() {

}

volatile uint8_t word = 0, refresh_counter = 0, data_from_mem_dat, data_from_mem_ref, address;

/**
    ISR obsluhujici externi presruseni INT0

    pro rizeni chovani radice se opet vyuziva stavova promenna instruction
    ta rika co bude radic v dalsim kroku delat
*/
ISR(INT0_vect) {
    if(PIND & (1 << 2)) { ///Pokud je CLK 1
        ///Dokonceni refreshe
        PORTB |= (1 << word); //Zvoleni adresy
        DDRC |= 0xFF; //Povoleni vystupudo pametoveho pole
        PORTC = data_from_mem_ref; //Zápis do pametoveho pole
        _delay_ms(10); //Aby se stihl nabit kondenzator

        PORTB &= 0xF0; //Reset adresy
        DDRC = 0; //Povolení čtení z paměti
        PORTC = 0; //Nulovanui vystupniho registru pametoveho pole

        word++; //Zmena adresy, ktera bude obnovena
        if(word == 4) { //Adresy se meni cyklycky
            word = 0;
        }

        ///Rohoduje se podle hodnoty instrukce
        switch(instruction) {
            case 2 : { //Reset linek do pametoveho pole
                //reset kontrolich vodicu pametoveho pole
                PORTB &= 0xF0; //Reset adresy
                PORTC = 0; //Nulovanui vystupniho registru pametoveho pole
                PINC = 0;  //Nulovanui vstupniho registru pametoveho pole
                DDRC = 0; //Povolení čtení z pametoveho pole

                instruction = 0; //Ukonci retezec instrukce
                break;
            }
        }

        ///Na radu prichazi signaly od procesoru
        if(CHIP_SELECT_PIN_REG & (1 << CHIP_SELECT_PIN)) { ///Chce procesor komunikovat?
            if(CONTROL_PIN_REG & (1 << CONTROL_PIN)) {
                ///Probiha cteni
                switch(instruction) {
                    case 4 : { //Zapis dat zpet do pametoveho pole
                        PORTB |= (1 << address); //Výběr adresy
                        DDRC |= 0xFF; //Povolení zápisu do pametoveho pole
                        PORTC = data_from_mem_dat; //Zápis do pametoveho pole

                        instruction = 5;
                        break;
                    }

                    default : { //Precte se adresa
                        address = adr_read();
                        instruction = 3;
                        break;
                    }
                }
            } else {
                ///Probiha zapis
                PORTB &= 0xF0; //Reset adresy
                PORTB |= (1 << adr_read()); //Vyber adresy
                DDRA = 0x00; //Prepnuti linek od procesoru na cteni
                DDRC |= 0xFF; //Povoleni zapisu do pametoveho pole
                PORTC = data_links_read(); //Zapis dat na vodice pametoveho pole
                instruction = 1;
            }
        }
    } else { ///Pokud je CLK 0
        ///Rozhoduje se podle instrukce
        switch(instruction) {
            case 1 : { //Ukonceni zapisu
                ///Reset linek do pameti
                PORTB &= 0xF0; //Reset adresy
                DDRC = 255; //Povolení zápisu do paměti
                PORTC = 0; //Nulovanui vystupniho registru pametoveho pole
                PINC = 0; //Nulovanui vstupniho registru pametoveho pole
                DDRC = 0; //Povolení čtení z paměti

                instruction = 0; //Konec instrukcniho retezce
                break;
            }

            case 3 : {
                /**Čtení z paměti a znovu zapsání*/
                DDRC = 255; //Povolení zápisu do paměti
                PORTC = 0;
                PINC = 0;
                DDRC = 0; //Povolení čtení z paměti
                PORTB &= 0xF0; //Reset adresy

                PORTB |= (1 << address); //Výběr adresy

                _delay_ms(2); //Cas pro otevreni MOSFETu

                data_from_mem_dat = 0; //Nulovani promenne - pokud selze cteni, "precte" se nula
                data_from_mem_dat = PINC; //Čtení z paměti

                DDRC = 255; //Povolení zápisu do paměti
                PORTC = 0;

                _delay_ms(2); //Cas na vybiti kondenzatoru - LED zhasnou

                DDRC = 0; //Zakazani zapisu do pameti

                ///Odeslani dat procesoru
                DDRA = 0xFF; //Povoleni zapisu na datove vodice do procesoru (skrz tistavove budice)
                PORTA = data_from_mem_dat; //Vystup na tristavove budice

                instruction = 4;
                break;
            }

            case 5 : { //Ukonceni zapisu do pametoveho pole
                ///Reset linek do pameti
                PORTB &= 0xF0; //Reset adresy
                DDRC = 0; //Povolení čtení z paměti
                PORTC = 0;

                ///reset datovych linek do procesoru
                PINA = 0;
                DDRA = 0;

                instruction = 0; //Konec instrukcniho retezce
                break;
            }
        }

        /**
            REFRESH - obnovovani naboje kondenzatoru v pameti
        */
        DDRC = 255; //Povolení zápisu do paměti
        PORTC = 0; //Nulovanui vystupniho registru pametoveho pole
        PINC = 0; //Nulovanui vstupniho registru pametoveho pole
        DDRC = 0; //Povolení čtení z paměti
        PORTB &= 0xF0; //Reset adresy
        PORTB |= (1 << word); //Výběr adresy

        _delay_ms(5);

        data_from_mem_ref = 0;
        data_from_mem_ref = PINC; //Čtení z paměti

        DDRC = 255; //Povolení zápisu do paměti

        PORTC = 0;
        _delay_ms(2); //I kdyz by se v ISR nemelo cekat, je to diky znalosti delky periody mozne
    }
}

/**
    Inicializace vsech komponent
*/
void init() {
    ///Neimplementovano
    //Init debug LED
    //DEBUG_LED_DDR |= 1 << DEBUG_LED_PIN;

    //Init CLK_SLAVE
    init_clk_slave();

    //Init memory addressation
    init_memory_select_address();

    //init komunikace - pouze DEBUG
    //init_rs232();
}

int main(void) {
    init();

    while(1) {
        ///Hlavni smycka je prazdana - vse pracuje synchronne s procesorem
    };

    return 0;
}
