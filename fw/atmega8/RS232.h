#ifndef RS232_H_INCLUDED
#define RS232_H_INCLUDED

uint8_t income_buffer_write(uint8_t data);
uint8_t income_buffer_read();
uint8_t send_buffer_write(uint8_t data);
uint8_t send_buffer_read();
void init_rs232();
uint8_t income_data();

#endif // RS232_H_INCLUDED
