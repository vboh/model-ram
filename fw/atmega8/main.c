/*
Copyright (C) 2017  Viktor Bohunek

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, go to:
https://www.gnu.org/licenses/old-licenses/gpl-2.0.html

ATmega8
*/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "RS232.h"

/**
    Deklarace promennych
*/
uint8_t i = 0;
volatile uint8_t instruction = 0, clk_counter = 0, instr_watchdog = 0, address, data_from_mem, data_to_mem;

/**
    Debug LED
    soubor definic a funkce, ktery umoznoval blikat diodou a pomahat tak zjistit stav programu

    VE FINALNI VERZI NEIMPLEMENTOANO
*/
#define DEBUG_LED_PIN 4
#define DEBUG_LED_DDR DDRD
#define DEBUG_LED_PORT PORTD
void debug_led() {
    DEBUG_LED_PORT ^= 1 << DEBUG_LED_PIN;
};

/**
    Adresni vodice
*/
///Inicializace HW
void init_adr_lines() {
    DDRB |= 0b00000011;
}

///Reset a nastaveni nove adresy
void adr_lines_set(uint8_t adr) {
    PORTB &= ~(0b00000011);
    PORTB |= adr & 0b00000011;
}

///reset adresacnich vodicu
void adr_lines_reset(){
    PORTB &= ~(0b00000011);
}

/**
    datove linky
*/

///Prepnuti do stavu pro cteni
void data_links_readable() {
    DDRD &= ~(0b11000000);
    DDRC &= ~(0b00111111);
}

///Preprnuti do stavu pro zapis
void data_links_writeable() {
    DDRD |= (0b11000000);
    DDRC |= (0b00111111);
}

///Zapis cisla na datove linky
void data_links_write(uint8_t value) {
    PORTD |= 0b11000000 & value;
    PORTC |= 0b00111111 & value;
}

///Cteni cisla z datovych linek
uint8_t data_links_read() {
    return (PIND & 0b11000000) | (PINC & 0b00111111);
}

///reset datovych linek
void data_links_reset() {
    PORTD &= ~(0b11000000);
    PORTC &= ~(0b00111111);
    data_links_readable();
}

///Nastaveni datovych linek na 0
void data_links_set_gnd() {
    data_links_writeable();
    PORTD &= ~(0b11000000);
    PORTC &= ~(0b00111111);
}

/**
    * Chip Select
    * Control
*/
#define CHIP_SELECT_PIN 2
#define CHIP_SELECT_DDR DDRD
#define CHIP_SELECT_PORT PORTD

#define CONTROL_PIN 3
#define CONTROL_DDR DDRD
#define CONTROL_PORT PORTD

/**
    Inicializace HW
*/
void init_CS_C() {
    CHIP_SELECT_DDR |= 1 << CHIP_SELECT_PIN;
    CONTROL_DDR |= 1 << CONTROL_PIN;
}

/**
    Zmena stavu Chip Select
*/
void chip_select(uint8_t value) {
    if(value == 0) {
        CHIP_SELECT_PORT &= ~(1 << CHIP_SELECT_PIN);
    } else {
        CHIP_SELECT_PORT |= (1 << CHIP_SELECT_PIN);
    }
}

/**
    Zmena stavu Control
*/
void control(uint8_t value) {
    if(value == 0) {
        CONTROL_PORT &= ~(1 << CONTROL_PIN);
    } else {
        CONTROL_PORT |= (1 << CONTROL_PIN);
    }
}

/**
CLK - Timer2

Nastaveni HW Timer2 tak, aby generoval frekvenci cca. 16 Hz a cca. 4 Hz (ukazka ztraty dat)
Povolene preruseni

Odpovidajici nastaveni timeru:
16 Hz:
15625 ticks, prescale 64, frequency 16 Hz, time 0.0625s => CLK_FREQ 8 Hz (CLK_BOTTOM 7)
CLK_TIMEOUT 62 + 1

4 Hz:
62500 ticks, prescale 64, frequency 4 Hz, time 0.25s => CLK_FREQ 4 Hz (CLK_BOTTOM 5)
CLK_TIMEOUT 244 + 1

Spravne delky periody je dosazeno kombinaci HW a SW ciatce:
    * citaci Timer2 se nastavuje pocatecni hodnota - tim se zkrati doba, nez pretece
    * v ISR preteceni je SW citac clk_counter, ktery se kontroluje na pocet preteceni,
    ten je nasobkem delky preteceni HW citace vypoctem

Tento zpusob umoznuje vytvorit temer libovolne dlouhou periodu, omezeni je dano:
    * prescale HW timeru (preddelicka frekvence z oscilatoru) pouziva mocniny dvou,
    oscilator ma ale spis frekvenci delitelnou desity a vznikaji tak nepresnosti
    * ne vzdy je mozne rozdelit periodu na rozumne mnozstvi preteceni HW timeru
*/

uint8_t clk_bottom[2] =  {7, 5}, //Nastaveni pro ruzne frekvence
        clk_timeout[2] = {63, 251}, //Nastaveni pro ruzne frekvence
        clk_freq = 0; //Rozliseni prave pouzivane frekvence

#define CLK_PIN 2
#define CLK_DDR DDRB
#define CLK_PORT PORTB

//inicializace Timer2 podle pozadovanych udaju
/*
    Registry viz. datasheet
*/
void init_clk() {
    cli();

    CLK_DDR |= 1 << CLK_PIN;

    TIMSK |= (1 << TOIE2);
    TCCR2 |=  (1 << CS22)
            ;
    TCNT2 = clk_bottom[clk_freq];

    sei();
}

//Obsluzna rutina preruseni preteceni Timer2
ISR(TIMER2_OVF_vect) {
    //Zajisteni SW ciatce
    TCNT2 = clk_bottom[clk_freq];
    clk_counter++;

    //Rizeni CLK
    if (clk_counter == clk_timeout[clk_freq]) {
        CLK_PORT ^= (1 << CLK_PIN);

        if(CLK_PORT & (1 << CLK_PIN)) { //Pokud je CLK 1
            switch (instruction) {
                case 9 : { //Cteni dat, ktere poslal radic a jejich preposlani kontrolnim SW
                    data_links_readable();
                    send_buffer_write(6);
                    send_buffer_write(data_links_read());
                    send_buffer_write(address);
                    instruction = 2;
                    break;
                }
            }
        } else { //Pokud je CLK 0

            switch (instruction) { //Nejake instukce?
                case 1 : { //Zapis prijateho cisla of kontrolniho SW
                    chip_select(1); //Vyber pameti
                    control(0); //Zapis do pameti
                    adr_lines_set(address); //Nastaveni adresy
                    data_links_writeable(); //Datalinks jako vystup
                    data_links_write(data_to_mem); //Nastavneni vystupu
                    instruction = 2;
                    break;
                }

                case 2 : { //ukonceni zapisu cisla
                    //Vse zresetovat
                    chip_select(0);
                    control(0);
                    adr_lines_reset();
                    data_links_reset();

                    send_buffer_write(4); //Odeslat OK
                    instruction = 0; //Ukoncit retezec instrukce
                    //data_links_set_gnd();
                    break;
                }

                case 7 : { //Zadani operace cteni radici
                    chip_select(1);
                    control(1);
                    adr_lines_set(address);
                    data_links_reset();
                    instruction = 8;
                    break;
                }

                case 8 : { //Cekani na precteni dat
                    data_links_reset();
                    instruction = 9;
                    break;
                }

            }

        }
        clk_counter = 0;
    }

    /**
        Implementace "watch dogu"
        kontroluje se, ze mezi prjetim bytu instrukce neuplyne prisli dlouha doba
    */
    if(instruction > 32) {
        instr_watchdog++;
        if(instr_watchdog == 50) { //Pokud neni instrukce vcas kompletni, odesle se ERROR
            instruction = 0;
            instr_watchdog = 0;
            send_buffer_write(5);
        }
    }
}

/**
    Inicializace vsech potrebnych HW komponent
*/
void init() {
    //Init debug LED - ve finalni verzi neimplementovano
    //DEBUG_LED_DDR |= 1 << DEBUG_LED_PIN;

    //Init RS232
    init_rs232();

    //init CLK timer - Timer2 - 8 Hz
    init_clk();

    //init Chip Select; Control
    init_CS_C();

    //init address lines
    init_adr_lines();
}

/**
    Hlavni smycka prijma data od kontrolnihop SW a predava je dal ke zpracovani
    Cely program pracuje na zaklade promenne instruction, ktera urcuje stav ve
    kterem se program nachazi, stavy na sebe navazuji a tvori tak retezec, ktery
    synchronne reaguje na vstupy a vystupy
*/
int main(void)
{
    init(); //Nutna inicializace

    while(1) { //Nelpnecna hlavni smycka
        switch (instruction) { //rozliseni stavu instrukce

            case 33 : { //Zapis
                if(income_data() > 1) { //Prisla adresa?
                    instr_watchdog = 0;
                    address = income_buffer_read(); //Adresa
                    data_to_mem = income_buffer_read(); //Zapis prichozich dat

                    instruction = 1;
                }
                break;
            }

            case 34 : { //Čteni
                if(income_data() > 0) { //Prisla adresa?
                    instr_watchdog = 0;
                    address = income_buffer_read(); //Adresa

                    send_buffer_write(4); //Potvrzeni prijeti instrukce

                    instruction = 7;
                }
                break;
            }

            case 40 : { //Zmena CLK
                if(income_data() > 0) { //Prislo oznaceni frekvence?
                    switch(income_buffer_read()) {
                        case 1 : {
                            clk_freq = 0;
                            send_buffer_write(4);
                            break;
                        }
                        case 2 : {
                            clk_freq = 1;
                            send_buffer_write(4);
                            break;
                        }
                        default : {
                            send_buffer_write(5);
                            break;
                        }
                    }
                    instruction = 0;
                }
                break;
            }

            default : { //Vyckavani
                if(income_data() > 0) { //Prisla instrukce
                    instr_watchdog = 0;
                    switch(income_buffer_read()) {
                        case 33 : {
                            instruction = 33; //Zapis
                            break;
                        }

                        case 34 : {
                            instruction = 34; //Cteni
                            break;
                        }

                        case 40 : {
                            instruction = 40; //Zmena CLK
                            break;
                        }
                    }

                }
                break;
            }
        }
    };

    return 0;
}
