#ifndef SERIAL_COMM_H_INCLUDED
#define SERIAL_COMM_H_INCLUDED

//Komentare k funkcim v souboru SERIAL_COMM.c

void write_to_comm(char *input, short bytes);
char get_com_ports(GtkTreeModel **tree_model);
gboolean open_com_port(gchar *com_port);
void close_com_port();
gboolean read_com_port (GIOChannel *source, GIOCondition condition, gpointer data);

///Specialni Windows funkce
#ifdef _WIN32
GSourceFunc dialog_response(gpointer user_data);
#endif // _WIN32

#endif // SERIAL_COMM_H_INCLUDED
