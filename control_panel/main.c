/*
Copyright (C) 2017  Viktor Bohunek

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, go to:
https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/

#define MAIN_FILE
#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include "response_functions.h"
#include "share.h"
#include "serial_comm.h"

/**
    Hlavni funkce

    nejprve jsou deklarovany promenne, do kterych se ulozi jednolive casti GUI
        zakomentovane promenne jsou deklarovany v share.h a jsou globalne
        pouzitelne napric vsemi soubory
    nasleduje inicalizace promenych a sestaveni GUI stromu
    Inicializuje se regulerni vyraz pro overovani dat v HEX formatu
*/

int main (int argc, char *argv[]) {
//Main window widgets
    //GtkWidget *window_main = NULL; - IN share.h
    GtkWidget *box_main = NULL;
    GtkWidget *scrolled_window_actions = NULL;

//Actions widgets
    GtkWidget *box_actions = NULL;
    GtkWidget *view_port_actions = NULL;

//Memory widgets
    GtkWidget *frame_memory = NULL;
    GtkWidget *grid_memory = NULL;
//    GtkWidget *label_memory[MEMORY_WORDS][MEMORY_WORD_LENGTH + 1];

//Widgety - vyber adresy
    GtkWidget *frame_address = NULL;
    GtkWidget *box_address = NULL;
    GtkWidget *label_address_dec = NULL;
    GtkWidget *box_address_dec = NULL;
    GtkWidget *spin_button_address_dec = NULL;
    GtkWidget *label_address_hex = NULL;
    GtkWidget *box_address_hex = NULL;
    GtkWidget *entry_address_hex = NULL;

//Widgety - Zapis
    GtkWidget *frame_write = NULL;
    GtkWidget *box_write = NULL;
    GtkWidget *box_write_dec = NULL;
    GtkWidget *label_write_dec = NULL;
    GtkWidget *spin_button_write_dec = NULL;
    GtkWidget *box_write_hex = NULL;
    GtkWidget *label_write_hex = NULL;
    GtkWidget *entry_write_hex = NULL;
//    GtkWidget *button_write = NULL;

//Widgety - Zapis FP
    GtkWidget *frame_write_fp = NULL;
    GtkWidget *box_write_fp = NULL;
    GtkWidget *box_write_fp_input = NULL;
    GtkWidget *label_write_fp = NULL;
    GtkWidget *entry_write_fp = NULL;
    GtkWidget *button_write_convert_fp = NULL;
    GtkWidget *box_write_fp_hex = NULL;
    GtkWidget *label_write_fp_hex = NULL;
    GtkWidget *entry_write_fp_hex = NULL;
//    GtkWidget *button_write_fp = NULL;

//Widgety - Ctení
    GtkWidget *frame_read = NULL;
    GtkWidget *box_read = NULL;
    GtkWidget *combo_box_read = NULL;
//    GtkWidget *button_read = NULL;

//Widgety - Zmena CLK
    GtkWidget *frame_clk = NULL;
    GtkWidget *box_clk = NULL;
    GtkWidget *combo_box_clk = NULL;
//    GtkWidget *button_clk;

//COM port control widgets
    GtkWidget *frame_com_port = NULL;
    GtkWidget *box_com_port = NULL;
//    GtkWidget *button_com_port_open = NULL;
//    GtkWidget *button_com_port_close = NULL;
//    GtkWidget *combo_box_com_port = NULL;
    GtkCellRenderer *combo_box_com_port_column = NULL;

//GErrors
    GError *error = NULL;

//Variables declaration
    unsigned char h = 0, v = 0;

//GTK INIT
    gtk_init(&argc, &argv);

//Main window init
    window_main = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window_main), "DRAM control panel");

//Memory widgets initialization
    frame_memory = gtk_aspect_frame_new("Memory", 0.5, 0.5, 2.181818, FALSE);
    gtk_widget_set_hexpand(GTK_WIDGET(frame_memory), TRUE);
    gtk_widget_set_vexpand(GTK_WIDGET(frame_memory), TRUE);

    //Definice CSS stylu pro labely
    css_green_bg = gtk_css_provider_new();
    gtk_css_provider_load_from_data(GTK_CSS_PROVIDER(css_green_bg), "* {background-color: #00FF00}", -1, NULL);

    css_red_bg = gtk_css_provider_new();
    gtk_css_provider_load_from_data(GTK_CSS_PROVIDER(css_red_bg), "* {background-color: #FF0000;}", -1, NULL);

    css_font = gtk_css_provider_new();
    gtk_css_provider_load_from_data(GTK_CSS_PROVIDER(css_font), "* {font-size: 10px;}", -1, NULL);

    //Tabulka pro pametové bunky
    grid_memory = gtk_grid_new();
    gtk_grid_set_row_spacing(GTK_GRID(grid_memory), 1);
    gtk_grid_set_column_spacing(GTK_GRID(grid_memory), 1);
    g_object_set(G_OBJECT(grid_memory), "margin", 10, NULL);

    for(v = 0; v < MEMORY_WORDS; v++)
    {
        for(h = 0; h < MEMORY_WORD_LENGTH + 1; h++)
        {

            if(h == 0) {
                if(v == 0) {
                    label_memory[v][h] = gtk_label_new(">");
                } else {
                    label_memory[v][h] = gtk_label_new("");
                }
            } else {
                label_memory[v][h] = gtk_label_new("0");
                gtk_style_context_add_provider(gtk_widget_get_style_context(label_memory[v][h]), GTK_STYLE_PROVIDER(css_red_bg), GTK_STYLE_PROVIDER_PRIORITY_USER);
            }

            gtk_style_context_add_provider(gtk_widget_get_style_context(label_memory[v][h]), GTK_STYLE_PROVIDER(css_font), GTK_STYLE_PROVIDER_PRIORITY_USER);
            gtk_widget_set_size_request(GTK_WIDGET(label_memory[v][h]), 50, 50);
            gtk_widget_set_hexpand(GTK_WIDGET(label_memory[v][h]), TRUE);
            gtk_widget_set_vexpand(GTK_WIDGET(label_memory[v][h]), TRUE);
            gtk_grid_attach(GTK_GRID(grid_memory), label_memory[v][h], h, v, 1, 1);
        }
    }

    //Zajisteni zmeny velikosti pisma
    g_signal_connect(G_OBJECT(label_memory[0][1]), "size-allocate", G_CALLBACK(size_allocate_label_memory), css_font);

    //Pridání do nadrazeného prvku
    gtk_container_add(GTK_CONTAINER(frame_memory), grid_memory);

//Inicializace widgetu - Vyber adresy
    label_address_dec = gtk_label_new("Address (DEC)");
    spin_button_address_dec = gtk_spin_button_new_with_range(0, 3, 1);
    gtk_widget_set_hexpand(GTK_WIDGET(spin_button_address_dec), TRUE);

    label_address_hex = gtk_label_new("Address (HEX)");
    entry_address_hex = gtk_entry_new();
    g_object_set(G_OBJECT(entry_address_hex), "sensitive", 0, NULL);
    gtk_entry_set_text(GTK_ENTRY(entry_address_hex), "0x0");
    gtk_entry_set_width_chars(GTK_ENTRY(entry_address_hex), 4);
    gtk_widget_set_hexpand(GTK_WIDGET(entry_address_hex), TRUE);

    box_address_dec = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    gtk_container_add(GTK_CONTAINER(box_address_dec), label_address_dec);
    gtk_container_add(GTK_CONTAINER(box_address_dec), spin_button_address_dec);

    box_address_hex = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    gtk_container_add(GTK_CONTAINER(box_address_hex), label_address_hex);
    gtk_container_add(GTK_CONTAINER(box_address_hex), entry_address_hex);

    box_address = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    g_object_set(G_OBJECT(box_address), "margin", 10, NULL);
    gtk_container_add(GTK_CONTAINER(box_address), box_address_dec);
    gtk_container_add(GTK_CONTAINER(box_address), box_address_hex);

    frame_address = gtk_frame_new("Address");
    gtk_container_add(GTK_CONTAINER(frame_address), box_address);


//Inicializace widgetu - Zapis
    label_write_dec = gtk_label_new("Data (DEC)");
    spin_button_write_dec = gtk_spin_button_new_with_range(0, 255, 1);
    gtk_widget_set_hexpand(GTK_WIDGET(spin_button_write_dec), TRUE);

    label_write_hex = gtk_label_new("Data (HEX)");
    entry_write_hex = gtk_entry_new();
    gtk_entry_set_width_chars(GTK_ENTRY(entry_write_hex), 4);
    gtk_entry_set_text(GTK_ENTRY(entry_write_hex), "0x0");
    gtk_widget_set_hexpand(GTK_WIDGET(entry_write_hex), TRUE);

    box_write_dec = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    gtk_container_add(GTK_CONTAINER(box_write_dec), label_write_dec);
    gtk_container_add(GTK_CONTAINER(box_write_dec), spin_button_write_dec);

    box_write_hex = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    gtk_container_add(GTK_CONTAINER(box_write_hex), label_write_hex);
    gtk_container_add(GTK_CONTAINER(box_write_hex), entry_write_hex);

    button_write = gtk_button_new_with_label("Write");

    box_write = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    g_object_set(G_OBJECT(box_write), "margin", 10, NULL);
    gtk_container_add(GTK_CONTAINER(box_write), box_write_dec);
    gtk_container_add(GTK_CONTAINER(box_write), box_write_hex);
    gtk_container_add(GTK_CONTAINER(box_write), button_write);

    frame_write = gtk_frame_new("Write");
    gtk_container_add(GTK_CONTAINER(frame_write), box_write);

    //Signaly
    struct widgets_write *widgets_write_normal = malloc(sizeof(widgets_write_normal));
    widgets_write_normal->data_in = spin_button_write_dec;
    widgets_write_normal->address_in = spin_button_address_dec;
    g_signal_connect(G_OBJECT(button_write), "clicked", G_CALLBACK(clicked_button_write), widgets_write_normal);

//Inicializace widgetu - Zapis FP
    label_write_fp = gtk_label_new("Data (DEC)");
    entry_write_fp = gtk_entry_new();

    label_write_fp_hex = gtk_label_new("Data (BIN)");
    entry_write_fp_hex = gtk_entry_new();
    gtk_entry_set_width_chars(GTK_ENTRY(entry_write_fp_hex), 8);
    gtk_entry_set_text(GTK_ENTRY(entry_write_fp_hex), "0x00000000");
    gtk_widget_set_hexpand(GTK_WIDGET(entry_write_fp_hex), TRUE);
    g_object_set(G_OBJECT(entry_write_fp_hex), "sensitive", 0, NULL);

    box_write_fp_input = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    gtk_container_add(GTK_CONTAINER(box_write_fp_input), label_write_fp);
    gtk_container_add(GTK_CONTAINER(box_write_fp_input), entry_write_fp);

    button_write_convert_fp = gtk_button_new_with_label("Convert FP");

    box_write_fp_hex = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    gtk_container_add(GTK_CONTAINER(box_write_fp_hex), label_write_fp_hex);
    gtk_container_add(GTK_CONTAINER(box_write_fp_hex), entry_write_fp_hex);

    button_write_fp = gtk_button_new_with_label("Write FP");

    box_write_fp = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    g_object_set(G_OBJECT(box_write_fp), "margin", 10, NULL);
    gtk_container_add(GTK_CONTAINER(box_write_fp), box_write_fp_input);
    gtk_container_add(GTK_CONTAINER(box_write_fp), button_write_convert_fp);
    gtk_container_add(GTK_CONTAINER(box_write_fp), box_write_fp_hex);
    gtk_container_add(GTK_CONTAINER(box_write_fp), button_write_fp);

    //Napojeni event handleru
    struct widgets_convert *widgets_callback_convert = malloc(sizeof(widgets_callback_convert));
    widgets_callback_convert->entry_write_fp = entry_write_fp;
    widgets_callback_convert->entry_write_fp_hex = entry_write_fp_hex;
    g_signal_connect(G_OBJECT(button_write_convert_fp), "clicked", G_CALLBACK(clicked_button_write_convert_fp), widgets_callback_convert);

    g_signal_connect(G_OBJECT(button_write_fp), "clicked", G_CALLBACK(clicked_button_write_fp), spin_button_address_dec);

    frame_write_fp = gtk_frame_new("Write FP");
    gtk_container_add(GTK_CONTAINER(frame_write_fp), box_write_fp);

//Inicializace widgetu - cteni
    combo_box_read = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo_box_read), "byte");
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo_box_read), "word");
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo_box_read), "dword");
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo_box_read), "float");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_box_read), 0);

    button_read = gtk_button_new_with_label("Read");

    box_read = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    g_object_set(G_OBJECT(box_read), "margin", 10, NULL);
    gtk_container_add(GTK_CONTAINER(box_read), combo_box_read);
    gtk_container_add(GTK_CONTAINER(box_read), button_read);

    //Napojeni signalu
    struct widgets_read *widgets_callback_read = malloc(sizeof(widgets_callback_read));
    widgets_callback_read->type_in = combo_box_read;
    widgets_callback_read->address_in = spin_button_address_dec;
    g_signal_connect(G_OBJECT(button_read), "clicked", G_CALLBACK(clicked_button_read), widgets_callback_read);

    frame_read = gtk_frame_new("Read");
    gtk_container_add(GTK_CONTAINER(frame_read), box_read);

//Inicializace widgetu - clk
    combo_box_clk = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo_box_clk), "8 Hz");
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo_box_clk), "2 Hz");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_box_clk), 0);

    button_clk = gtk_button_new_with_label("Set CLK");

    box_clk = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    g_object_set(G_OBJECT(box_clk), "margin", 10, NULL);
    gtk_container_add(GTK_CONTAINER(box_clk), combo_box_clk);
    gtk_container_add(GTK_CONTAINER(box_clk), button_clk);

    frame_clk = gtk_frame_new("Set CLK");
    gtk_container_add(GTK_CONTAINER(frame_clk), box_clk);

    //Signaly
    g_signal_connect(G_OBJECT(button_clk), "clicked", G_CALLBACK(clicked_button_clk), combo_box_clk);


//COM port control widgets initialization
    frame_com_port = gtk_frame_new("COM port");
    box_com_port = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    g_object_set(G_OBJECT(box_com_port), "margin", 10, NULL);

    combo_box_com_port = gtk_combo_box_new();
    combo_box_com_port_column = gtk_cell_renderer_text_new();
    gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combo_box_com_port), combo_box_com_port_column, TRUE);
    gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(combo_box_com_port), combo_box_com_port_column,  "text", 1,  NULL);
    gtk_combo_box_set_id_column(GTK_COMBO_BOX(combo_box_com_port), 0);

    button_com_port_open = gtk_button_new_with_label("Open COM port");
    button_com_port_close = gtk_button_new_with_label("Close COM port");
    gtk_container_add(GTK_CONTAINER(box_com_port), combo_box_com_port);
    gtk_container_add(GTK_CONTAINER(box_com_port), button_com_port_open);
    gtk_container_add(GTK_CONTAINER(box_com_port), button_com_port_close);
    gtk_container_add(GTK_CONTAINER(frame_com_port), box_com_port);

    //Signaly
    g_signal_connect(G_OBJECT(button_com_port_open), "clicked", G_CALLBACK(clicked_button_com_port_open), NULL);
    g_signal_connect(G_OBJECT(button_com_port_close), "clicked", G_CALLBACK(clicked_button_com_port_close), NULL);

//Main window widgets initialization
    box_main = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    g_object_set(G_OBJECT(box_main), "margin", 10, NULL);

    box_actions = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    gtk_widget_set_hexpand(GTK_WIDGET(box_actions), FALSE);

//Sestaveni boxu s ovladacimi prvky
    gtk_container_add(GTK_CONTAINER(box_actions), frame_address);
    gtk_container_add(GTK_CONTAINER(box_actions), frame_write);
    gtk_container_add(GTK_CONTAINER(box_actions), frame_write_fp);
    gtk_container_add(GTK_CONTAINER(box_actions), frame_read);
    gtk_container_add(GTK_CONTAINER(box_actions), frame_clk);
    gtk_container_add(GTK_CONTAINER(box_actions), frame_com_port);

    view_port_actions = gtk_viewport_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(view_port_actions), box_actions);

    scrolled_window_actions = gtk_scrolled_window_new (NULL, NULL);
    gtk_widget_set_size_request(GTK_WIDGET(scrolled_window_actions), 275, -1);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window_actions),GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_container_add(GTK_CONTAINER(scrolled_window_actions), view_port_actions);

//Sestaveni okna
    gtk_container_add(GTK_CONTAINER(box_main), frame_memory);
    gtk_container_add(GTK_CONTAINER(box_main), scrolled_window_actions);
    gtk_container_add(GTK_CONTAINER(window_main), box_main);

//Zmìna hodnoty adresy
    g_signal_connect(G_OBJECT(spin_button_address_dec), "value-changed", G_CALLBACK(changed_spin_button_address_dec), entry_address_hex);

//Synchronizace hodnoty "Data" na vstupu - zápis
    g_signal_connect(G_OBJECT(spin_button_write_dec), "value-changed", G_CALLBACK(changed_spin_button_write_dec), entry_write_hex);
    g_signal_connect(G_OBJECT(entry_write_hex), "changed", G_CALLBACK(changed_entry_write_hex), spin_button_write_dec);
    g_signal_connect(G_OBJECT(spin_button_write_dec), "focus-out-event", G_CALLBACK(focus_out_event_spin_button_write_dec), entry_write_hex);

//Zakazani ovládacích prvků
    g_object_set(button_read, "sensitive", 0, NULL);
    g_object_set(button_write, "sensitive", 0, NULL);
    g_object_set(button_clk, "sensitive", 0, NULL);
    g_object_set(button_write_fp, "sensitive", 0, NULL);
    g_object_set(button_com_port_close, "sensitive", 0, NULL);

//Add icon to main window
    //ikona ze zdroje: https://pixabay.com/en/chip-computer-processor-152652/  Licence CC0
    gtk_window_set_icon_from_file(GTK_WINDOW(window_main), "icon.png", &error);
    if(error != NULL) {
        g_print(error->message);
        g_free(error);
        error = NULL;
    }

//Init COM port combo box
    GtkTreeModel *tree_model;
    if(get_com_ports(&tree_model)) {
        gtk_combo_box_set_model(GTK_COMBO_BOX(combo_box_com_port), tree_model);
        gtk_combo_box_set_active(GTK_COMBO_BOX(combo_box_com_port), 0);
    } else {
        show_error("RS-232 Error: No COM ports found");
        return -1;
    }

//Regex initialization
    regex_hexadecimal = g_regex_new("(^((0x)[A-Fa-f0-9]{1,2})$)|(^0x$)", G_REGEX_OPTIMIZE | G_REGEX_DOLLAR_ENDONLY, 0, &error);
    if(error != NULL) {
        g_print(error->message);
        g_free(error);
        error = NULL;
    }

//End of inits
    g_signal_connect(window_main, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_show_all(window_main);

#ifdef _WIN32
    //Fuknce na platforme WIN reaguje na akce z druheho vlakna, ktere resi cteni z RS-232
    g_timeout_add(10, (GSourceFunc)(dialog_response), NULL);
#endif

///Start programu
gtk_main();

    return 0;
}

