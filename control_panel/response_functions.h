#ifndef RESPONSE_FUNCTIONS_H_INCLUDED
#define RESPONSE_FUNCTIONS_H_INCLUDED

//Komentare k funkcimv souboru RESPONSE_FUNCTIONS.c

void show_error(gchar *error);

void clicked_button_com_port_open(GtkButton *button, gpointer *user_data);
void clicked_button_com_port_close(GtkButton *button, gpointer *user_data);

void changed_spin_button_address_dec(GtkSpinButton *spin_button, gpointer user_data);

void size_allocate_label_memory(GtkWidget *widget, GdkRectangle *allocation, gpointer user_data);

void clicked_button_write_convert_fp(GtkButton *button, gpointer user_data);

void changed_spin_button_write_dec(GtkSpinButton *spin_button, gpointer user_data);
void changed_entry_write_hex(GtkEditable *editable, gpointer user_data);
void focus_out_event_spin_button_write_dec(GtkWidget *widget, GdkEvent  *event, gpointer user_data);

void clicked_button_write(GtkButton *button, gpointer *user_data);
void clicked_button_write_fp(GtkButton *button, gpointer *user_data);
void clicked_button_read(GtkButton *button, gpointer *user_data);

void control_widgets_set_sensitivity(gint sensitivity);

void show_byte(unsigned char address, unsigned char byte);

void clicked_button_clk(GtkButton *button, gpointer *user_data);

#endif // RESPONSE_FUNCTIONS_H_INCLUDED
