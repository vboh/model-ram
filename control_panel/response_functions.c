/*
Copyright (C) 2017  Viktor Bohunek

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, go to:
https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/

#include <gtk/gtk.h>
#include <stdlib.h>
#include <math.h>
#include "serial_comm.h"
#include "share.h"
#include "response_functions.h"

/**
    zde jsousdruzeny vsechny funkce, ktere reaguji na GUI
*/

/**
    Vypise dialog s chybou, obaluje GTK funkci gtk_message_dialog_new()
*/
void show_error(gchar *error) {
    GtkWidget *dialog;
    dialog = gtk_message_dialog_new(GTK_WINDOW(window_main),
        GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_ERROR,
        GTK_BUTTONS_CLOSE,
        error);
    gtk_window_set_title(GTK_WINDOW(dialog), "Error");
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

/**
    Reakce na kliknuti na tlactko pro otevreni COM-portu
*/
void clicked_button_com_port_open(GtkButton *button, gpointer *user_data) {
    GtkTreeIter iter;
    gchar *com_port;
    gtk_combo_box_get_active_iter(GTK_COMBO_BOX(combo_box_com_port), &iter);

    gtk_tree_model_get(GTK_TREE_MODEL(gtk_combo_box_get_model(GTK_COMBO_BOX(combo_box_com_port))), &iter, 1, &com_port, -1);

    if(open_com_port(com_port)) {
        //Pokud se povedlo otevrit COM-port, povoli se ovladaci prvky
        g_object_set(button_com_port_open, "sensitive", 0, NULL);
        g_object_set(combo_box_com_port, "sensitive", 0, NULL);
        g_object_set(button_read, "sensitive", 1, NULL);
        g_object_set(button_write, "sensitive", 1, NULL);
        g_object_set(button_clk, "sensitive", 1, NULL);
        g_object_set(button_write_fp, "sensitive", 1, NULL);
        g_object_set(button_com_port_close, "sensitive", 1, NULL);
    } else {
        //V opacnem pripade se vypise error
        show_error("RS-232 Error: Cannot open COM port");
    }

}

/**
    Reakce na kliknuti na tlacitko pro zavreni COM-portu
*/
void clicked_button_com_port_close(GtkButton *button, gpointer *user_data) {
    close_com_port();
    g_object_set(button_com_port_open, "sensitive", 1, NULL);
    g_object_set(combo_box_com_port, "sensitive", 1, NULL);
    g_object_set(button_read, "sensitive", 0, NULL);
    g_object_set(button_write, "sensitive", 0, NULL);
    g_object_set(button_clk, "sensitive", 0, NULL);
    g_object_set(button_write_fp, "sensitive", 0, NULL);
    g_object_set(button_com_port_close, "sensitive", 0, NULL);
}

/**
    Pri zmene dekadicke adresy se prevede na hex tvar a vlozi do policka
    a zmena pozice ukazatele na vybrany radek
*/
void changed_spin_button_address_dec(GtkSpinButton *spin_button, gpointer user_data) {
    GtkWidget *entry_address_hex = (GtkWidget *)user_data;
    char str[4];
    int address = gtk_spin_button_get_value_as_int(spin_button);
    sprintf(str, "%s%X", "0x", address);
    gtk_entry_set_text(GTK_ENTRY(entry_address_hex), str);

    int i;
    for(i = 0; i < MEMORY_WORDS; i++) {
        if(i == address) {
            gtk_label_set_text(GTK_LABEL(label_memory[i][0]), ">");
        } else {
            gtk_label_set_text(GTK_LABEL(label_memory[i][0]), "");
        }
    }
}

void size_allocate_label_memory(GtkWidget *widget, GdkRectangle *allocation, gpointer user_data) {
    GtkCssProvider *css_font = (GtkCssProvider *)user_data;
    char css[30];
    sprintf(css, "%s%d%s", "* {font-size: ", allocation->width / 2, "px;}");
    gtk_css_provider_load_from_data(GTK_CSS_PROVIDER(css_font), css, -1, NULL);
}

/**
    Zobrazi detaily konverze - znaménko, exponent, mantisu
*/
void show_dialog_floating_point_conversion(gpointer window, float float_number) {
    GtkWidget *dialog;

    int number = *(int *)&float_number;
    number_floating_point = number;

//Očištění čísla na mantisu - pro výpočet a zobrazení
    int int_float_number = *((int *)&float_number);
    int_float_number |= 0b00111111100000000000000000000000;
    int_float_number &= ~(0b11000000000000000000000000000000);
    float float_mantisa = *((float*)&int_float_number);

    int sign, exponent, mantisa;

    sign = ((number & 0x80000000) >> 31);
    exponent = (number & 0b01111111100000000000000000000000) >> 23;
    mantisa = number & 0b00000000011111111111111111111111;

//Dekódované znaménko
    char s_sign[2];
    sprintf(s_sign, "%d", sign);

//Dekódovaný exponent (bez korekce)
    char s_d_exponent[5];
    sprintf(s_d_exponent, "%d", exponent);

//Dekódovaný exponent (s korekcí)
    char s_dk_exponent[5];
    sprintf(s_dk_exponent, "%d", (exponent - 127));

//Dekódovaná mantisa - celé číslo
    char s_d_mantisa[10];
    sprintf(s_d_mantisa, "%d", mantisa);

//Dekódovaná mantisa jako desetinné číslo
    char s_float_mantisa[30];
    sprintf(s_float_mantisa, "%f", float_mantisa);

    int i, j;
//Dekódovaný exponent - binárně
    char s_exponent[9];
    for(i = 7, j = 0; i >= 0; i--) {
        if(exponent & (1 << i)) {
            s_exponent[j++] = '1';
        } else {
            s_exponent[j++] = '0';
        }
    }
    s_exponent[8] = '\0';

//Dekódovaná mantisa - binárně
    char s_mantisa[24];
    for(i = 22, j = 0; i >= 0; i--) {
        if(mantisa & (1 << i)) {
            s_mantisa[j++] = '1';
        } else {
            s_mantisa[j++] = '0';
        }
    }
    s_mantisa[23] = '\0';

//Znovu spočítané číslo
    float number_reverse = pow(-1, sign) * pow(2, exponent - 127) * float_mantisa;
    char s_number_reverse[30];
    sprintf(s_number_reverse, "%f", number_reverse);

    dialog = gtk_message_dialog_new(GTK_WINDOW(window),
        GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_INFO,
        GTK_BUTTONS_CLOSE,
        g_strconcat("Sign: ", s_sign, " => ", (sign == 0) ? "+" : "-",
                    "\nExponent: ", s_exponent, " => ", s_d_exponent,
                    "\nMantisa: ", s_mantisa, " => ", s_d_mantisa,
                    "\n -1^", s_sign, " * 2^", s_dk_exponent, " * ", s_float_mantisa, " = ", s_number_reverse,
                    NULL));
    gtk_window_set_title(GTK_WINDOW(dialog), "Floating point conversion");
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

/**
    Reakce na kliknuti na tlacitko pro prevod na bianrni desetinne cislo
    Vola funkci pro zobrazeni detailu konverze
    zapise hodnotu v hexa tvaru do policka
*/
void clicked_button_write_convert_fp(GtkButton *button, gpointer user_data) {
    struct widgets_convert *data = (struct widgets_convert *)user_data;
    char fp[12];
    float number;
    sscanf(gtk_entry_get_text(GTK_ENTRY(data->entry_write_fp)), "%e", &number);
    sprintf(fp, "%s%X", "0x", *(int *)&number);
    gtk_entry_set_text(GTK_ENTRY(data->entry_write_fp_hex), fp);

    show_dialog_floating_point_conversion(window_main, number);
}

/**
    Nasledujici tri funkce synchronizuji policko dekadickeho zapisu
    bytu a hexadecimalniho zapisu bytu
*/
/**
    Reakce nazmenu DEC hodnoty, aktualizace policka s HEX hodnotou
*/
void changed_spin_button_write_dec(GtkSpinButton *spin_button, gpointer user_data) {
    if(gtk_widget_has_focus(GTK_WIDGET(spin_button))) {
        char val[5];
        sprintf(val, "%s%X", "0x", gtk_spin_button_get_value_as_int(spin_button));
        gtk_entry_set_text((GtkEntry *)user_data, val);
    }
}

/**
    Reakce na zmenu HEX, neprve se kontroluje tvar a pak se konvertuje na DEC a zapise do policka
*/
void changed_entry_write_hex(GtkEditable *editable, gpointer user_data) {
    if(g_regex_match(regex_hexadecimal, gtk_entry_get_text(GTK_ENTRY(editable)), 0, NULL)) {
        int val;
        sscanf(gtk_entry_get_text(GTK_ENTRY(editable)), "%x", &val);
        gtk_spin_button_set_value((GtkSpinButton *)user_data, val);
    } else {//Pokud tvar neodpovida, zapise se 0
        gtk_entry_set_text(GTK_ENTRY(editable), "0x0");
        gtk_editable_set_position(editable, 3);
        gtk_spin_button_set_value((GtkSpinButton *)user_data, 0);
    }
}

/**
    Osetreni stavu, kdy pri opusteni policka pro DEC nedojde k aktualizaci hodnoty a nezavola se
    vyse popsana funkce
    opet se DEC hodnota prevede na HEX
*/
void focus_out_event_spin_button_write_dec(GtkWidget *widget, GdkEvent  *event, gpointer user_data) {
    gtk_spin_button_update(GTK_SPIN_BUTTON(widget));
    char val[5];
    sprintf(val, "%s%X", "0x", gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widget)));
    gtk_entry_set_text(GTK_ENTRY((GtkEntry *)user_data), val);
}

/**
    Reakce na kliknuti na tlacitko pro zapis

*/
void clicked_button_write(GtkButton *button, gpointer *user_data) {
    struct widgets_write *input = (struct widgets_write*) user_data; //Prevedeni null pointeru na struct pro pouziti vlozenych promennych
    gtk_spin_button_update(GTK_SPIN_BUTTON(input->data_in));
    gtk_spin_button_update(GTK_SPIN_BUTTON(input->address_in));
    int address = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(input->address_in));
    int data = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(input->data_in));

    show_byte(address, data); //Zobrazi byte v memory grid

    command = 1; //stav programu
    unsigned char output[3] = {33, address, data};
    write_to_comm(output, 3);
}

///Podpurna fce, povoluje ci zakazuje kontrolni prvky pro ovladani modelu behem operaci
void control_widgets_set_sensitivity(gint sensitivity) {
    g_object_set(button_read, "sensitive", sensitivity, NULL);
    g_object_set(button_write, "sensitive", sensitivity, NULL);
    g_object_set(button_clk, "sensitive", sensitivity, NULL);
    g_object_set(button_write_fp, "sensitive", sensitivity, NULL);
}

/**
    Reakce na kliknuti na tlacitko zapis desetinneho cisla
    Zapis FP cisla do pameti, odstartuje se rutina, ktera pokracuje diky stavove promenne
    command v dalsim vlakne, ktere reaguje na prectene data
*/
void clicked_button_write_fp(GtkButton *button, gpointer *user_data) {
    gtk_spin_button_update(GTK_SPIN_BUTTON(user_data));
    operation_adr = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(user_data));
    unsigned char data;
    data = (unsigned char) (number_floating_point >> (byte_iter * 8));
    byte_iter = 0;

    show_byte((byte_iter + operation_adr), data); //Zobrazi byte v memory grid

    command = 2;
    byte_iter = 0;
    char output[3] = {33, (byte_iter + operation_adr), data};
    write_to_comm(output, 3);
    byte_iter++;
}

/**
    Reakce na kliknuti na tlacitko cteni
    urci se typ cteni a spusti se rutina, ktera diky promenne command
    pokracuje ve vlakne cteni z RS-232
*/
unsigned char type_to_command[4] = {3,4,5,6};

void clicked_button_read(GtkButton *button, gpointer *user_data) {
    struct widgets_read *input = (struct widgets_read *) user_data;
    unsigned char type = gtk_combo_box_get_active(GTK_COMBO_BOX(input->type_in));
    operation_adr = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(input->address_in));

    /**
    0 - byte (3)
    1 - word (4)
    2 - dword (5)
    3 - float (6)
    */
    command = type_to_command[type];
    byte_iter = 0;
    unsigned char output[2] = {34, (operation_adr + byte_iter)};
    write_to_comm(output, 2);
}

///Pomocna funkce, ktera zobrazi zapsany byte v poli memory_grid
void show_byte(unsigned char address, unsigned char byte) {
    int i, b = MEMORY_WORD_LENGTH - 1;
    for(i = 1; i < MEMORY_WORD_LENGTH + 1; i++) {
        if(byte & (1 << b)) {
            if(g_strcmp0(gtk_label_get_text(GTK_LABEL(label_memory[address][i])), "0") == 0) { //Pokud se str rovnaji, vrci strcmp 0 !!!
                gtk_style_context_remove_provider(gtk_widget_get_style_context(label_memory[address][i]), GTK_STYLE_PROVIDER(css_red_bg));
                gtk_style_context_add_provider(gtk_widget_get_style_context(label_memory[address][i]), GTK_STYLE_PROVIDER(css_green_bg), GTK_STYLE_PROVIDER_PRIORITY_USER);
                gtk_label_set_text(GTK_LABEL(label_memory[address][i]), "1");
            }
        } else {
            if(g_strcmp0(gtk_label_get_text(GTK_LABEL(label_memory[address][i])), "1") == 0) { //Pokud se str rovnaji, vrci strcmp 0 !!!
                gtk_style_context_remove_provider(gtk_widget_get_style_context(label_memory[address][i]), GTK_STYLE_PROVIDER(css_green_bg));
                gtk_style_context_add_provider(gtk_widget_get_style_context(label_memory[address][i]), GTK_STYLE_PROVIDER(css_red_bg), GTK_STYLE_PROVIDER_PRIORITY_USER);
                gtk_label_set_text(GTK_LABEL(label_memory[address][i]), "0");
            }
        }
        b--;
    }
}

/**
Reakce na kliknuti na talcitko "Set CLK"
*/
void clicked_button_clk(GtkButton *button, gpointer *user_data) {
    GtkWidget *combo_box = (GtkWidget*) user_data;
    command = 7;
    unsigned char output[2] = {40, (gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box)) + 1)};
    write_to_comm(output, 2);
}













