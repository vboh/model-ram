#ifndef SHARE_H_INCLUDED
#define SHARE_H_INCLUDED

///Tento soubor slouzi pro sdileni promennych mezi jednotlivymi soubory programu

//SHARED CONSTANTS
#define MEMORY_WORDS 4
#define MEMORY_WORD_LENGTH 8

//Structs pro sdílení skrz user_data callbacků
///K zápisu hodnoty FP čísla po provedení konverze
struct widgets_convert {
    GtkWidget *entry_write_fp;
    GtkWidget *entry_write_fp_hex;
};

struct widgets_write {
    GtkWidget *data_in;
    GtkWidget *address_in;
};

struct widgets_read {
    GtkWidget *type_in;
    GtkWidget *address_in;
};

//Globální deklarace
#ifdef  MAIN_FILE
//Regex
GRegex *regex_hexadecimal;
GtkWidget *window_main;

GtkCssProvider* css_font;
GtkCssProvider* css_red_bg;
GtkCssProvider* css_green_bg;

GtkWidget *button_com_port_open;
GtkWidget *button_com_port_close;
GtkWidget *button_write;
GtkWidget *button_read;
GtkWidget *button_clk;
GtkWidget *combo_box_com_port;
GtkWidget *button_write_fp;

GtkWidget *label_memory[MEMORY_WORDS][MEMORY_WORD_LENGTH + 1];

//Promene pro RS-232
/**
command:
0 - nic
1 - zapis bytu
2 - zapis float
3 - cteni bytu
4 - cteni word
5 - cteni dword
6 - cteni float
7 - zmena CLK
*/
int command = 0, number_floating_point = 0;
unsigned char byte_iter = 0, operation_adr = 0;

#else

//Regex
extern GRegex *regex_hexadecimal;
extern GtkWidget *window_main;

extern GtkCssProvider* css_font;
extern GtkCssProvider* css_red_bg;
extern GtkCssProvider* css_green_bg;

extern GtkWidget *button_com_port_open;
extern GtkWidget *button_com_port_close;
extern GtkWidget *button_write;
extern GtkWidget *button_read;
extern GtkWidget *button_clk;
extern GtkWidget *combo_box_com_port;
extern GtkWidget *button_write_fp;

extern GtkWidget *label_memory[MEMORY_WORDS][MEMORY_WORD_LENGTH + 1];

//Promene pro RS-232
extern int command, number_floating_point;
extern unsigned char byte_iter, operation_adr;

#endif // MAIN_FILE
#endif // SHARE_H_INCLUDED
