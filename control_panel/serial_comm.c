/*
Copyright (C) 2017  Viktor Bohunek

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, go to:
https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/

#include <gtk/gtk.h>
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <unistd.h>

#include "share.h"
#include "response_functions.h"
#include "serial_comm.h"

int number_read;
unsigned char byte_iter;

/**
    Zobrazi dialog s prectenymi daty
*/
void show_read_data(char *string) {
    GtkWidget *dialog;
    dialog = gtk_message_dialog_new(GTK_WINDOW(window_main),
                                    GTK_DIALOG_DESTROY_WITH_PARENT,
                                    GTK_MESSAGE_INFO,
                                    GTK_BUTTONS_CLOSE,
                                    string);
    switch (command) {
        case 3 : {
            gtk_window_set_title(GTK_WINDOW(dialog), "Data - byte");
            break;
        }

        case 4 : {
            gtk_window_set_title(GTK_WINDOW(dialog), "Data - word");
            break;
        }

        case 5 : {
            gtk_window_set_title(GTK_WINDOW(dialog), "Data - dword");
            break;
        }

        case 6 : {
            gtk_window_set_title(GTK_WINDOW(dialog), "Data - float");
            break;
        }
    }
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

///Pro POSIXove systemy
#if defined(__linux__) || defined(__FreeBSD__)
#include <termios.h> /// POSIX terminal control definitions

int com_port_descriptor;
GIOChannel *com_port_gioch;

/**
    Cteni z COM-portu
    Pokud se cte vice bytove cislo, musi program reagovat na prichozi data
    dalsim pozadavkem na cteni
    Pokud se zapisuje vice bytove cislo, opet je nutne na spravne zapsane cislo
    reagovat dalsim pozdackem na zapis
*/
gboolean read_com_port (GIOChannel *source, GIOCondition condition, gpointer data) {
    gsize bytes_read;
    unsigned char input[3];

    if(g_io_channel_read_chars(source, input, 1, &bytes_read, NULL) == G_IO_STATUS_NORMAL) {
        switch(command) {
            case 1 : { //zapis bytu
                switch (input[0]) {
                    case 4 : { //OK
                        command = 0;
                        control_widgets_set_sensitivity(1);
                        break;
                    }

                    case 5 : {//ERROR
                        command = 0;
                        show_error("RS-232 Error: Problem with communication (Code: 1-5)");
                        control_widgets_set_sensitivity(1);
                        break;
                    }

                    default : {
                        command = 0;
                        show_error("RS-232 Error: General communication error (Code: 1-d)");
                        control_widgets_set_sensitivity(1);
                        break;
                    }
                }
                break;
            }

            case 2 : {//Zapis float
                switch (input[0]) {
                    case 4 : {//OK
                        if(byte_iter < 4) {
                            unsigned char data;
                            data = (unsigned char) (number_floating_point >> (byte_iter * 8));

                            show_byte((byte_iter + operation_adr), data); //Zobrazi byte v memory grid

                            char output[3] = {33, (byte_iter + operation_adr), data};
                            write_to_comm(output, 3);
                            byte_iter++;
                        } else {
                            byte_iter = 0;
                            command = 0;
                            control_widgets_set_sensitivity(1);
                        }
                        break;
                    }

                    case 5 : {
                        command = 0;
                        show_error("RS-232 Error: Problem with communication (Code: 2-5)");
                        control_widgets_set_sensitivity(1);
                        break;
                    }

                    default : {
                        command = 0;
                        show_error("RS-232 Error: General communication error (Code: 2-d)");
                        control_widgets_set_sensitivity(1);
                        break;
                    }
                }
                break;
            }

            case 3 : { //cteni bytu
                switch (input[0]) {
                    case 4 : { //OK
                        if(g_io_channel_read_chars(source, input, 1, &bytes_read, NULL) == G_IO_STATUS_NORMAL) {
                            if(input[0] == 6) {
                                if(g_io_channel_read_chars(source, input, 3, &bytes_read, NULL) == G_IO_STATUS_NORMAL) {
                                    if(input[2] == 4) { //Po poslednim bytu dojde k vytvoreni dialogu a zobrazeni uzivateli
                                        char adr[5], data_h[4], data_d[3];
                                        sprintf(adr, "%X", input[1]);
                                        sprintf(data_h, "%X", input[0]);
                                        sprintf(data_d, "%d", input[0]);
                                        show_read_data(g_strconcat("Data: 0x", data_h, "\nData: ", data_d, "\nFrom address: 0x", adr, NULL));
                                        command = 0;
                                        byte_iter = 0;
                                    } else {
                                        command = 0;
                                        show_error("RS-232 Error: Problem with communication (Code: 3-4-lOK)");
                                        control_widgets_set_sensitivity(1);
                                    }
                                } else {
                                    command = 0;
                                    show_error("RS-232 Error: General communication error (Code: 3-4-D)");
                                    control_widgets_set_sensitivity(1);
                                }
                            }
                        } else {
                            command = 0;
                            show_error("RS-232 Error: General communication error (Code: 3-4)");
                            control_widgets_set_sensitivity(1);
                        }
                        command = 0;
                        control_widgets_set_sensitivity(1);
                        break;
                    }

                    case 5 : {//ERROR
                        command = 0;
                        show_error("RS-232 Error: Problem with communication (Code: 3-5)");
                        control_widgets_set_sensitivity(1);
                        break;
                    }

                    default : {
                        command = 0;
                        show_error("RS-232 Error: General communication error (Code: 3-d)");
                        control_widgets_set_sensitivity(1);
                        break;
                    }
                }
                break;
            }


            case 4 : { //cteni word
                switch (input[0]) {
                    case 4 : { //OK
                        if(g_io_channel_read_chars(source, input, 1, &bytes_read, NULL) == G_IO_STATUS_NORMAL) {

                            if(input[0] == 6) {

                                if(g_io_channel_read_chars(source, input, 3, &bytes_read, NULL) == G_IO_STATUS_NORMAL) {

                                    if(input[2] == 4) {
                                        number_read |= (input[0] << (byte_iter * 8));
                                        byte_iter++;

                                        if(byte_iter < 2) {
                                            unsigned char output[2] = {34, (operation_adr + byte_iter)};
                                            write_to_comm(output, 2);
                                        } else { //Po poslednim bytu dojde k vytvoreni dialogu a zobrazeni uzivateli
                                            char adr[5], data_h[5], data_d[6];
                                            sprintf(adr, "%X", operation_adr);
                                            sprintf(data_h, "%X", number_read);
                                            sprintf(data_d, "%d", number_read);
                                            show_read_data(g_strconcat("Data: 0x", data_h, "\nData: ", data_d, "\nFrom address: 0x", adr, NULL));
                                            command = 0;
                                            byte_iter = 0;
                                            control_widgets_set_sensitivity(1);
                                        }



                                    } else {
                                        command = 0;
                                        show_error("RS-232 Error: Problem with communication (Code: 4-4-lOK)");
                                        control_widgets_set_sensitivity(1);
                                    }
                                } else {
                                    command = 0;
                                    show_error("RS-232 Error: General communication error (Code: 4-4-D)");
                                    control_widgets_set_sensitivity(1);
                                }
                            }
                        } else {
                            command = 0;
                            show_error("RS-232 Error: General communication error (Code: 4-4)");
                            control_widgets_set_sensitivity(1);
                        }
                        break;
                    }



                    case 5 : {//ERROR
                        command = 0;
                        show_error("RS-232 Error: Problem with communication (Code: 4-5)");
                        control_widgets_set_sensitivity(1);
                        break;
                    }

                    default : {
                        command = 0;
                        show_error("RS-232 Error: General communication error (Code: 4-d");
                        control_widgets_set_sensitivity(1);
                        break;
                    }
                }
                break;
            }

            case 5 : { //cteni dword
                switch (input[0]) {
                    case 4 : { //OK
                        if(g_io_channel_read_chars(source, input, 1, &bytes_read, NULL) == G_IO_STATUS_NORMAL) {

                            if(input[0] == 6) {

                                if(g_io_channel_read_chars(source, input, 3, &bytes_read, NULL) == G_IO_STATUS_NORMAL) {

                                    if(input[2] == 4) {
                                        number_read |= (input[0] << (byte_iter * 8));
                                        byte_iter++;

                                        if(byte_iter < 4) {
                                            unsigned char output[2] = {34, (operation_adr + byte_iter)};
                                            write_to_comm(output, 2);
                                        } else { //Po poslednim bytu dojde k vytvoreni dialogu a zobrazeni uzivateli
                                            char adr[5], data_h[9], data_d[11];
                                            sprintf(adr, "%X", operation_adr);
                                            sprintf(data_h, "%X", number_read);
                                            sprintf(data_d, "%d", number_read);
                                            show_read_data(g_strconcat("Data: 0x", data_h, "\nData: ", data_d, "\nFrom address: 0x", adr, NULL));
                                            command = 0;
                                            byte_iter = 0;
                                            control_widgets_set_sensitivity(1);
                                        }



                                    } else {
                                        command = 0;
                                        show_error("RS-232 Error: Problem with communication (Code: 4-4-lOK)");
                                        control_widgets_set_sensitivity(1);
                                    }
                                } else {
                                    command = 0;
                                    show_error("RS-232 Error: General communication error (Code: 4-4-D)");
                                    control_widgets_set_sensitivity(1);
                                }
                            }
                        } else {
                            command = 0;
                            show_error("RS-232 Error: General communication error (Code: 4-4)");
                            control_widgets_set_sensitivity(1);
                        }
                        break;
                    }



                    case 5 : {//ERROR
                        command = 0;
                        show_error("RS-232 Error: Problem with communication (Code: 4-5)");
                        control_widgets_set_sensitivity(1);
                        break;
                    }

                    default : {
                        command = 0;
                        show_error("RS-232 Error: General communication error (Code: 4-d");
                        control_widgets_set_sensitivity(1);
                        break;
                    }
                }
                break;
            }

            case 6 : { //cteni float
                switch (input[0]) {
                case 4 : { //OK
                    if(g_io_channel_read_chars(source, input, 1, &bytes_read, NULL) == G_IO_STATUS_NORMAL) {

                        if(input[0] == 6) {

                            if(g_io_channel_read_chars(source, input, 3, &bytes_read, NULL) == G_IO_STATUS_NORMAL) {

                                if(input[2] == 4) {
                                    number_read |= (input[0] << (byte_iter * 8));
                                    byte_iter++;

                                    if(byte_iter < 4) {
                                        unsigned char output[2] = {34, (operation_adr + byte_iter)};
                                        write_to_comm(output, 2);
                                    } else { //Po poslednim bytu dojde k vytvoreni dialogu a zobrazeni uzivateli
                                        char adr[5], data_h[9], data_d[11];
                                        sprintf(adr, "%X", operation_adr);
                                        sprintf(data_h, "%X", number_read);
                                        float number_float = *((float*)&number_read);;
                                        sprintf(data_d, "%f", number_float);
                                        show_read_data(g_strconcat("Data: 0x", data_h, "\nData: ", data_d, "\nFrom address: 0x", adr, NULL));
                                        command = 0;
                                        control_widgets_set_sensitivity(1);
                                    }

                                } else {
                                    command = 0;
                                    show_error("RS-232 Error: Problem with communication (Code: 4-4-lOK)");
                                    control_widgets_set_sensitivity(1);
                                }
                            } else {
                                command = 0;
                                show_error("RS-232 Error: General communication error (Code: 4-4-D)");
                                control_widgets_set_sensitivity(1);
                            }
                        }
                    } else {
                        command = 0;
                        show_error("RS-232 Error: General communication error (Code: 4-4)");
                        control_widgets_set_sensitivity(1);
                    }
                    break;
                }



                case 5 : {//ERROR
                    command = 0;
                    show_error("RS-232 Error: Problem with communication (Code: 4-5)");
                    control_widgets_set_sensitivity(1);
                    break;
                }

                default : {
                    command = 0;
                    show_error("RS-232 Error: General communication error (Code: 4-d");
                    control_widgets_set_sensitivity(1);
                    break;
                }
                }
                break;
            }

            case 7 : { //Zmena CLK
                switch(input[0]) {
                    case 4 : {
                        command = 0;
                        control_widgets_set_sensitivity(1);
                        break;
                    }

                    case 5 : {//ERROR
                        command = 0;
                        show_error("RS-232 Error: Problem with communication (Code: 7-5)");
                        control_widgets_set_sensitivity(1);
                        break;
                    }

                    default : {
                        command = 0;
                        show_error("RS-232 Error: General communication error (Code: 7-d");
                        control_widgets_set_sensitivity(1);
                        break;
                    }
                }
                break;
            }

            default : {
                command = 0;
                show_error("RS-232 Error: General communication error (Code: SG)");
                control_widgets_set_sensitivity(1);
                break;
            }
        }
    } else {
        command = 0;
        show_error("RS-232 Error: General communication error (Code: G)");
        control_widgets_set_sensitivity(1);
    }
    return 1;
}

/**
    Otevrení COM-portu a jeho nastavení
*/
gboolean open_com_port(gchar *com_port) {
    com_port_descriptor = open(g_strconcat("/dev/", com_port, NULL), O_RDWR | O_NOCTTY | O_NDELAY);
    if(com_port_descriptor > 0) {
        fcntl(com_port_descriptor, F_SETFL, 0);

        struct termios options;

        /*
         * Get the current options for the port...
         */

        tcgetattr(com_port_descriptor, &options);

        /*
         * Set the baud rates to 19200...
         */

        cfsetispeed(&options, B9600);
        cfsetospeed(&options, B9600);

        /*
         * Enable the receiver and set local mode...
         */

        options.c_cflag |= (CLOCAL | CREAD);

        //8N1
        options.c_cflag &= ~PARENB;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS8;

        //Raw input
        options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

        //Raw output
        options.c_oflag &= ~OPOST;

        //One BYTE is enough to return from read
        options.c_cc[VMIN]  = 1;

        //Inter-character timer OFF
        options.c_cc[VTIME] = 0;

        /*
         * Set the new options for the port...
         */

        tcsetattr(com_port_descriptor, TCSANOW, &options);

        ///Vytvoreni GIOChannelu
        com_port_gioch = g_io_channel_unix_new(com_port_descriptor);
        g_io_add_watch(com_port_gioch, G_IO_IN, read_com_port, NULL); //Sledovani stavu com-portu
        g_io_channel_set_encoding(com_port_gioch, NULL, NULL);
        return 1;
    }
    show_error(g_strconcat("RS-232 Error: ", strerror(errno), NULL));
    return 0;
}

/**
    Zapis od COM-portu
*/
void write_to_comm(char *input, short bytes) {
    int n = 0;
    n = write(com_port_descriptor, input, bytes);
    if(n < 0) {
        show_error("RS-232 Error: Can't write to COM port");
    } else {
        control_widgets_set_sensitivity(0);
    }
}

/**
    Uzavrit COM-port
*/
void close_com_port() {
    close(com_port_descriptor);
}

/**
    Funkce vytvori gtk_tree_model ze vsech COM-portu; na POSIx systemech
    se projde specialni adresar a typ zarizeni se urci z vlastnosti
    jednotlivych souboru v adresari
*/
char get_com_ports(GtkTreeModel **tree_model) {
    GDir *dir = NULL;
    GError *dir_error = NULL;
    GtkTreeIter iter;

    GtkListStore *list = gtk_list_store_new(2, G_TYPE_INT, G_TYPE_STRING);
    const gchar *name;
    gchar *path = "/sys/class/tty/", *device = "/device", *result_path;
    int i = 0;

    dir = g_dir_open("/sys/class/tty/", 0, &dir_error);
    if(dir_error != NULL) {
        show_error(dir_error->message);
        g_free(dir_error);
        return 0;
    }

    while((name = g_dir_read_name(dir)) != NULL) {
        dir_error = NULL;
        result_path = g_strconcat(path, name, device, NULL);
        g_dir_open(result_path, 0, &dir_error);
        if(dir_error != NULL) {
            dir_error = NULL;
        } else {
            gtk_list_store_append(list, &iter);
            gtk_list_store_set(list, &iter,  0, i++,  1, name, -1);
        }
        g_free(result_path);
    }
    if(dir_error != NULL) {
        g_error_free(dir_error);
    }
    g_dir_close(dir);
    *tree_model =  GTK_TREE_MODEL(list);
    return 1;
}

#else //WIN32
#include <windows.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <share.h>
#include <io.h>
#include <pthread.h>

char *data_string, *error_string;
unsigned char new_error = 0, new_data = 0, pth_state;
pthread_t *pth;

HANDLE com_port_handle = INVALID_HANDLE_VALUE;

/**
    Zapis dat do COM-portu
*/
void write_to_comm(char *input, short bytes) {
    long unsigned int n = 0;
    WriteFile(com_port_handle, input, bytes, &n, NULL);
    if(n == 0) {
        show_error("RS-232 Error: Can't write to COM port");
    } else {
        control_widgets_set_sensitivity(0);
    }
}

/**
    Funkce vytahne ze systemovych registru potrebne klice, ze kterych zjisti nazvy pripojenych COM-portu
    z nich vytvori gtk_tree_model
*/
char get_com_ports(GtkTreeModel **tree_model) {
    GtkTreeIter iter;
    HKEY h_key;
    GtkListStore *list = gtk_list_store_new(2, G_TYPE_INT, G_TYPE_STRING);
    int value_iterator = 0;
    long error_code;
    unsigned long max_value_name_len, max_value_data_len;

    error_code = RegOpenKeyEx(HKEY_LOCAL_MACHINE, "HARDWARE\\DEVICEMAP\\SERIALCOMM", 0, KEY_QUERY_VALUE, &h_key);
    if(error_code != ERROR_SUCCESS) {
        show_error("RS-232 Error: No COM ports found");
        return 0;
    }

    error_code = RegQueryInfoKeyA(h_key, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &max_value_name_len, &max_value_data_len, NULL, NULL);
    if(error_code != ERROR_SUCCESS) {
        char e[6];
        sprintf(e, "%d", error_code);
        show_error(g_strconcat("Regitry Error: Invalid entry (Code: ", e, ")", NULL));
        return 0;
    }

    max_value_name_len += 1;

    char *value_name;
    unsigned char *value_data;
    value_name = calloc(1, sizeof(char) * max_value_name_len);
    unsigned long value_name_len = max_value_name_len;
    value_data = calloc(1, sizeof(unsigned char) * max_value_data_len);
    unsigned long value_data_len = max_value_data_len;

    while(error_code != ERROR_NO_MORE_ITEMS) {
        error_code = RegEnumValueA(h_key, value_iterator, value_name, &value_name_len, NULL, NULL, value_data, &value_data_len);
        if(error_code == ERROR_SUCCESS) {
            gtk_list_store_append(list, &iter);
            gtk_list_store_set(list, &iter,  0, value_iterator,  1, value_data, -1);
            value_name_len = max_value_name_len;
            value_iterator++;
        } else if(error_code != ERROR_NO_MORE_ITEMS) {
            char e[6];
            sprintf(e, "%d", error_code);
            show_error(g_strconcat("Regitry Error: Read entry error (Code: ", e, ")", NULL));
        }
    }

    RegCloseKey(h_key);
    free(value_data);
    free(value_name);

    if(value_iterator == 0) {
        return 0;
    }

    *tree_model = GTK_TREE_MODEL(list);
    return 1;
}

/**
    Cteni z COM-portu
    Pokud se cte vice bytove cislo, musi program reagovat na prichozi data
    dalsim pozadavkem na cteni
    Pokud se zapisuje vice bytove cislo, opet je nutne na spravne zapsane cislo
    reagovat dalsim pozdackem na zapis

    Tato funkce je hlavni funkci dalsiho vlakna, ktere je nutne kvuli zpusobu prace
    Windows s COM-porty spustit. Vlakno pouze v nekonecne smycce cte data z COM-portu
    a reaguje na ne. Vlakno lze ukoncit zmenou promene pth_state, kdy se nekonecna
    smycka prerusi a vlakno se ukonci. System reagovani je uzpusoben tomu, ze byty
    se ctou po jednoma muselo tak vzniknout vice mezistavu nez, kdyz je mozne cekat
    az dorazi dostatek bytu s pomoci GIOChannel
*/
void *start() {
    unsigned char status = 0;
    char adr[5], data_h[12], data_d[15];
    unsigned char input[3];
    unsigned int t, attempt = 0;
    long unsigned int read_bytes = 0;

    while(pth_state) {
        if (com_port_handle != INVALID_HANDLE_VALUE) {
            ReadFile(com_port_handle, &input, 1, &read_bytes, NULL);

            if(status > 0) {
                attempt++;
                if(attempt > 100) {
                    status = 0;
                    attempt = 0;
                    new_error = 1;
                    error_string = "RS-232 Error: General communication error (Code: 0a)";
                }
            }

            if(read_bytes > 0) {
                attempt = 0;
                switch(command) {
                case 1 : { //zapis bytu
                    switch (input[0]) {
                    case 4 : { //OK
                        command = 0;
                        control_widgets_set_sensitivity(1);
                        break;
                    }

                    case 5 : {//ERROR
                        new_error = 1;
                        status = 0;
                        error_string = "RS-232 Error: Problem with communication (Code: 1-5)";
                        break;
                    }

                    default : {
                        new_error = 1;
                        status = 0;
                        error_string = "RS-232 Error: General communication error (Code: 1-d)";
                        break;
                    }
                    }
                    break;
                }

                case 2 : {//Zapis float
                    switch (input[0]) {
                    case 4 : {//OK
                        if(byte_iter < 4) {
                            unsigned char data;
                            data = (unsigned char) (number_floating_point >> (byte_iter * 8));

                            if((byte_iter + operation_adr) < MEMORY_WORDS) {
                                show_byte((byte_iter + operation_adr), data); //Zobrazi byte v memory grid
                            }
                            char output[3] = {33, (byte_iter + operation_adr), data};
                            write_to_comm(output, 3);
                            byte_iter++;
                        } else {
                            byte_iter = 0;
                            command = 0;
                            control_widgets_set_sensitivity(1);
                        }
                        break;
                    }

                    case 5 : {
                        new_error = 1;
                        status = 0;
                        error_string = "RS-232 Error: Problem with communication (Code: 2-5)";
                        break;
                    }

                    default : {
                        new_error = 1;
                        status = 0;
                        error_string = "RS-232 Error: General communication error (Code: 2-d)";
                        break;
                    }
                    }
                    break;
                }

                case 3 : { //cteni bytu
                    switch(status) {
                    default : {
                        if(input[0] == 4) {
                            status = 31;
                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 3-0)";
                            control_widgets_set_sensitivity(1);
                        }
                        break;
                    }

                    case 31 : {
                        if(input[0] == 6) {
                            status = 32;
                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 3-1)";
                            control_widgets_set_sensitivity(1);
                        }
                        break;
                    }

                    case 32 : {
                        sprintf(data_h, "%X", input[0]);
                        sprintf(data_d, "%d", input[0]);
                        status = 33;
                        break;
                    }

                    case 33 : {
                        sprintf(adr, "%X", input[0]);
                        status = 34;
                        break;
                    }

                    case 34 : {
                        if(input[0] == 4) {
                            new_data = 1;
                            data_string = g_strconcat("Data: 0x", data_h, "\nData: ", data_d, "\nFrom address: 0x", adr, NULL);
                            status = 0;
                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 3-4)";
                        }
                        break;
                    }
                    }
                    break;
                }


                case 4 : { //cteni bytu
                    switch(status) {
                    default : {
                        if(input[0] == 4) {
                            status = 41;
                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 4-0)";
                        }
                        break;
                    }

                    case 41 : {
                        if(input[0] == 6) {
                            status = 42;
                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 4-1)";
                        }
                        break;
                    }

                    case 42 : {
                        number_read |= (input[0] << (8 * byte_iter));
                        status = 43;
                        break;
                    }

                    case 43 : {
                        if(byte_iter == 0) {
                            sprintf(adr, "%X", input[0]);
                        }
                        status = 44;
                        break;
                    }

                    case 44 : {
                        if(input[0] == 4) {

                            if(byte_iter == 1) {
                                sprintf(data_h, "%X", number_read);
                                sprintf(data_d, "%d", number_read);

                                new_data = 1;
                                data_string = g_strconcat("Data: 0x", data_h, "\nData: ", data_d, "\nFrom address: 0x", adr, NULL);
                                status = 0;
                                number_read = 0;
                                byte_iter = 0;
                            } else {
                                byte_iter++;
                                status = 45;

                                char output[2] = {34, (byte_iter + operation_adr)};
                                write_to_comm(output, 2);
                            }

                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 4-4)";
                        }
                        break;
                    }
                    }
                    break;
                }

                case 5 : { //cteni bytu
                    switch(status) {
                    default : {
                        if(input[0] == 4) {
                            status = 51;
                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 5-0)";
                        }
                        break;
                    }

                    case 51 : {
                        if(input[0] == 6) {
                            status = 52;
                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 5-1)";
                        }
                        break;
                    }

                    case 52 : {
                        number_read |= (input[0] << (8 * byte_iter));
                        status = 53;
                        break;
                    }

                    case 53 : {
                        if(byte_iter == 0) {
                            sprintf(adr, "%X", input[0]);
                        }
                        status = 54;
                        break;
                    }

                    case 54 : {
                        if(input[0] == 4) {

                            if(byte_iter == 3) {
                                sprintf(data_h, "%X", number_read);
                                sprintf(data_d, "%d", number_read);

                                new_data = 1;
                                data_string = g_strconcat("Data: 0x", data_h, "\nData: ", data_d, "\nFrom address: 0x", adr, NULL);
                                status = 0;
                                number_read = 0;
                                byte_iter = 0;
                            } else {
                                byte_iter++;
                                status = 55;

                                char output[2] = {34, (byte_iter + operation_adr)};
                                write_to_comm(output, 2);
                            }

                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 5-4)";
                        }
                        break;
                    }
                    }
                    break;
                }


                case 6 : { //cteni bytu
                    switch(status) {
                    default : {
                        if(input[0] == 4) {
                            status = 61;
                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 5-0)";
                        }
                        break;
                    }

                    case 61 : {
                        if(input[0] == 6) {
                            status = 62;
                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 5-1)";
                        }
                        break;
                    }

                    case 62 : {
                        number_read |= (input[0] << (8 * byte_iter));
                        status = 63;
                        break;
                    }

                    case 63 : {
                        if(byte_iter == 0) {
                            sprintf(adr, "%X", input[0]);
                        }
                        status = 64;
                        break;
                    }

                    case 64 : {
                        if(input[0] == 4) {

                            if(byte_iter == 3) {
                                sprintf(data_h, "%X", number_read);
                                float number_float = *((float*)&number_read);;
                                sprintf(data_d, "%f", number_float);

                                new_data = 1;
                                data_string = g_strconcat("Data: 0x", data_h, "\nData: ", data_d, "\nFrom address: 0x", adr, NULL);
                                status = 0;
                                number_read = 0;
                                byte_iter = 0;
                            } else {
                                byte_iter++;
                                status = 65;

                                char output[2] = {34, (byte_iter + operation_adr)};
                                write_to_comm(output, 2);
                            }

                        } else {
                            new_error = 1;
                            status = 0;
                            error_string = "RS-232 Error: Problem with communication (Code: 6-4)";
                        }
                        break;
                    }
                    }
                    break;
                }

                case 7 : {
                    switch(input[0]) {
                        case 4 : {
                            command = 0;
                            control_widgets_set_sensitivity(1);
                            break;
                        }

                        case 5 : {//ERROR
                            command = 0;
                            show_error("RS-232 Error: Problem with communication (Code: 7-5)");
                            control_widgets_set_sensitivity(1);
                            break;
                        }

                        default : {
                            command = 0;
                            show_error("RS-232 Error: General communication error (Code: 7-d");
                            control_widgets_set_sensitivity(1);
                            break;
                        }
                    }
                    break;
                }

                default : {
                    show_error("RS-232 Error: General communication error (Code: SG)");
                    g_print("%d; ", input[0]);
                    break;
                }
                }
            }
        }
    }
    pthread_exit(NULL);
    return 0;
}

/**
    Funkce otevre COM-port a nastavi partametry linky, pokud se vse povede, spusti dalsi vlakno, ktere
    pouze cte a reaguje na prichozi byty
*/
gboolean open_com_port(gchar *com_port) {

    com_port_handle = CreateFile(g_strconcat("\\\\.\\", com_port, NULL), //port name
                                 GENERIC_READ | GENERIC_WRITE, //Read/Write
                                 0,                            // No Sharing
                                 NULL,                         // No Security
                                 OPEN_EXISTING,// Open existing port only
                                 0,            // Non Overlapped I/O
                                 NULL);        // Null for Comm Devices

    if (com_port_handle != INVALID_HANDLE_VALUE) {

        DCB dcb;

        GetCommState(com_port_handle, &dcb);

        dcb.BaudRate = CBR_9600;
        dcb.ByteSize = 8;
        dcb.Parity = NOPARITY;
        dcb.StopBits = ONESTOPBIT;
        dcb.fAbortOnError = FALSE;

        // set XON/XOFF
        dcb.fOutX	= FALSE;
        dcb.fInX	= FALSE;
        // set RTSCTS
        dcb.fOutxCtsFlow = FALSE;
        dcb.fRtsControl = RTS_CONTROL_DISABLE;
        // set DSRDTR
        dcb.fOutxDsrFlow = FALSE;
        dcb.fDtrControl = DTR_CONTROL_DISABLE;

        SetCommState(com_port_handle, &dcb);

        COMMTIMEOUTS timeouts;
        timeouts.ReadIntervalTimeout = 10;
        timeouts.ReadTotalTimeoutConstant = 10;
        timeouts.ReadTotalTimeoutMultiplier = 1;
        timeouts.WriteTotalTimeoutMultiplier = 0;
        timeouts.WriteTotalTimeoutConstant = 0;

        if (!SetCommTimeouts(com_port_handle, &timeouts)) {
            show_error("RS-232 Error: Error seting up timeouts for COM port");
        } else {
            pth_state = 1;
            pthread_create(pth, NULL, start, NULL);
        }
        return 1;
    }
    return 0;
}

/**
    Zavre COM-port a ukonci cteci vlakno
*/
void close_com_port() {
    pth_state = 0;
    CloseHandle(com_port_handle);
}

/**
    Tato funkce je cyklycky spoustena kazdych 10 ms (viz main)
    reaguje na dve promenne ktere signalizuji, ze druhe vlakno chce zobrazit bud error dialog nebo
    ze chce zobrazit prichozi data
*/
GSourceFunc dialog_response(gpointer user_data) {
    if(new_error == 1) {
        show_error(error_string);
        control_widgets_set_sensitivity(1);

        new_error = 0;
    }

    if(new_data == 1) {
        show_read_data(data_string);
        command = 0;
        control_widgets_set_sensitivity(1);

        new_data = 0;
    }

    return G_SOURCE_CONTINUE;
}

#endif

