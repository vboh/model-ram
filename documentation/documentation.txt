﻿Popis a rozbor reálné DRAM


Popis buňky

1: https://ecee.colorado.edu/~bart/book/book/chapter7/ch7_6.htm
2: http://www.eng.utah.edu/~cs6810/pres/12-6810-15.pdf
3: http://www.eng.utah.edu/~cs7810/pres/11-7810-12.pdf

Paměť typu DRAM je jedním ze základních typů polovodičových pamětí. Základem je tzv. buňka, která zvládne uložit jeden bit. Buňka paměti DRAM se skládá z jednoho mosfetu a jednoho kondenzátoru. (OBR: chéma buňky) Z technologických důvodů má kondenzátor velmi malou kapacitu 1fF (Zdr. 1). Právě kvůli kondenzátoru, jehož náboj se postupně vybíjí do okolí a je tedy nutné jej po určitém čase obnovit, se tanto typ paměti nazývá dynamický.

Kvůli obnovování je paměť energeticky náročná, navíc data mizí po odopojení od zdroje (volatilní), na druhou stranu je ale možné levně a snadno dosáhnout velké hustoty informaci právě díky jednoduchosti buněk.

Buňky jsou dále spojeny do matice pomocí dvou signálů. Signál bit-line je připojen k source mosfetu a slouží pro manipulaci s nábojem kondenzátoru. Druhý signál, word-line, je připojen k gate mosfetu a určuje buňky, s nimiž bude pracovat bit-line.


Popis modulu DIMM
Dnes je DRAm v počítačích organizována do paměťových modulů typu DIMM. Jeden DIMM má dnes standartně osazeno 16 paměťových čipů. DIMM se logicky dělí na 1 až 4 tzv. ranks, dnes běžně 2. Z předchozího plyne, že běžně se rank skládá z 8 čipů. Rank se dále dělí na 4 až 16 tzv. banks. Bank se dále dělí na několik polí, standartně 8, protože pole se na výsledné datové šířce podílí jedním bitem.

Způsob adresování bytu

Přímá adresace je nemožná. Délka adresy je proto rozdělena. Nejprve je určen rank, potom bank. Adresace v rámci pole je rozdělena do dvou kroků. Nejprve vybereme řádek a potom sloupec. Tyto dva kroky trvají celkem dlouho a určují výkonost paměti. Jedná se o RAS, RAS-to-CAS a CAS latence. Cím kratší jsou tyto časy, tím více I/O operací paměť zvládne.

Konkrétní příklad adresace
Pro ukázku použijeme adresu o délce 32 bitů. Máme jen jeden DIMM s 2 ranks, který každý obsahuje 4 banks. Adresa se tedy skládá z 1 bitu pro rank, ze dvou bitů pro bank. Zbytek je rozdělen na polovinu, tedy 14, toto číslo odpovídá počtu řádků a sloupců matice, ze které se skládají pole v banks.


Rozbor modelu
Na úvod je nutné podoknout, že model se nesažil znázornit moderní paměť DRAM v celé její komplikovanosti. Model zcela opomíjí logickou i fyzickou strukturu DIMM. S pamětí DRAM se zcela shoduje pouze buňka. Model se tedy ani nemůže držet složitého systému adresování u DIMM, to bylo nahrazeno přímou adresací.

Na stavbu byly v maximální možné míře použity diskrétní součástky a na všech důležitých signálech a v buňkách jsou navíc LED, které velmi přehledně zobrazují logický stav. Kvůli těmto úpravám došlo ke zjednodušení adresace na přímou, což na druhou stranu zlepšilo přehlednost celého procesu adresace. Dalším důsledkem je snížení taktovací frekvence na pouhých 16 Hz. Model se díky těmto zjednodušením na straně DRAM dokáže věnovat i procesům okolo paměti. Názorně je předvedena práce procesoru s pamětí od začátku až do konce. Čemuž pomáhá i ovládací software pro počítač.

Model je tedy schopný prakticky ukázat řadu procesů, které se vyučují, jmenovitě: přímou adresaci bytu, výběr čipu, 