# README

## Výukový model RW-DRAM

* Paměťové pole o velikosti 32 bitů
* Řízení pomocí počítače skrz RS232 TTL
* Názorně předvádí práci s pamětí na HW úrovni
    * Adresace
    * Tok dat
    * Chip select
    * Control
    * Třístavové buzení

## Chci vyrobit model i pro svou školu

Není problém, všechny zdroje dávám volně k dispozici pod GNU-GPL licencí

### Co je potřeba sehnat

* Toolchain
    * [avr-gcc](http://www.atmel.com/tools/atmelavrtoolchainforwindows.aspx) - není třeba, pokud použijete správné mikrokontroléry
    * [avrdude](http://download.savannah.gnu.org/releases/avrdude/)
* Programátor mikrokontrolérů Atmel ATmega 
    * Osobně jsem použil čínský USBASP a vlastnoručně zkonstruovaný USBtinyISP od Adafruit, jako poslední možnost lze použít Arduino jako ISP
* Výrobce plošných spojů (vzhledem k množství je to nejlepší varianta) 

### Rámcový postup

* Vyrobit plošné spoje (složka hw)
    * Plošné spoje byly navrženy v programu EAGLE od Autodesk
* Osadit plošné spoje (seznamy součástek sestavit podle schémat)
* Pořídit desku, na kterou se umístí tištěné spoje
    * Zvolil jsem běžnou dřevotřísku s tloušťou 20 mm a použil jsem běžně dostupné vruty
* Na podkladní desku se umístí převodník na RS232, řídící deska, propojovací deska paměťového pole, všechny připojovací desky třístavových budičů a signální deska
* Propojit všechny desky
    * Paměťové pole
        * připojit k napájení na řídící desce
        * adresační vodiče (horní vstup paměťového pole k výstupu řadiče úplně v pravo, pokud je klíče na pouzdru vlevo)
        * datové vodiče (spojit přímo s výstupy řadiče za odpory)
    * Převodník RS232
        * RX (třetí výstup z prava, když je konektor USB vlevo) na vstup u Atmega8 vlevo (pokud je klíč na pouzdru vpravo)
        * TX (druhý výstup z prava) na vstup hned vedle vpravo
        * GND (čtvrtý výstup z prava) na poslední konektor v řadě, vedle připojení TX
    * Třístavové budiče
        * mezi jednotlivými budiči propojíme úplně horní konektory z horní řady a všechny tři konektory odspodu ze spodní sady (spodní sada je ta, kde jsou 4 konetory, horní ta s 2 konektory)
        * poslední datový konektor ze sady vpravo (ten nejnižší, pokud je deska natočena tak, že v pravém horním rohu jsou 3 vodorovné konektory) připojíme k prvnímu budiči na zbylý konektor v horní řadě
        * první konektor zleva od řadiče připojíme k prvnímu budiči k na horní konektor spodní sady
            * podobně postupujeme u dalších budičů
        * první budič připojíme na napájení, VCC přivedeme na úplně spodní konektor spodní sady a na konektor nad ním GND
        * první budič připojíme na řídící signály sběrnice, vedle odporů u konektorů paměťového pole připojíme levý výstup na úplně horní konektor horní sady a pravý výstup na třetí konektor odspodu na spodní sadě
    * Signalizační deska
        * horní konektor ze sady vlevo na konektor vpravo u výstupů na Atmega8 
        * levý konektor ze tří vodorovných nahoře na pravý konektor ze tří v levém dolním rohu řídící desky
        * horní konektor ze dvou svyslých na levý konektor vedle odporů u konektorů pro paměťové pole, spodní do pravého
        * levý konektor ze spodní vodorovné řady na GND a pravý na VCC
* Naprogramovat mikrokontroléry pomocí programátoru s použitím souborů v e sloužkách Release u jednotlivých mikrokontrolérů
* Do počítače nainstalovat GTK+
    * Buď stáhnout instalátor vývojového prostředí [Vala](https://sourceforge.net/projects/valainstaller/)
    * Nebo pouze [knihovny](https://ulozto.cz/!KJw2trap6zXr/gtk-zip) a rozbalit, do stejné složky pak vložit soubory z dalšího kroku
* Stáhnout ovládací SW ze složky Release-WIN (control_panel/bin) soubory dram-control-panel.exe, icon.png a libwinpthread-1.dll
* Připojit model a otestovat

### Autor

* Viktor Bohuněk
* SPŠE a VOŠ Pardubice, Česká Republika
